# SimonComputing Developer Tutorials

These tutorials are for basic developer setup with suggested best practices.   Note that these tutorials were created for developers using Macs.  For Windows users, you can follow along by using [GitBash](https://git-for-windows.github.io/) which gives you a Unix like command line.   

## Developer Environment Setup

1. [Suggested Directory Structure](./setup/directory-structure.md)
1. [Git](./setup/git.md)
1. [Visual Studio Code](./setup/visual-studio-code.md)
1. [Eclipse](./setup/eclipse.md)
1. [Node & NPM](./setup/node.md)
1. [Yarn](./setup/yarn.md)
1. [Angular CLI](./setup/angular.md)
1. [Maven](./setup/maven-setup.md)
1. [Gradle](./setup/gradle.md)

## Skills

1. [Using Git](./skills/using-git.md)
1. [TypeScript](https://www.typescriptlang.org/docs/tutorial.html) 
1. [Angular 4 Training](./skills/angular/README.md)
1. [SpringBoot REST Service](./skills/spring-rest-service.md)
2. [Basic Web App with Gradle](https://guides.gradle.org/building-java-web-applications/)

