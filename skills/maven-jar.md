# Building a Basic Jar

This is a manual approach to building a JAR or Java ARchive.   This can be used to build both client applications and library code.   The advantages of this approach are:
 1. You gain a deeper understanding of the structure of the application
 1. You are using the current specifications instead of relying on defaults created by Eclipse or a Maven archetype that are out of date.

## Create the basic project structure	

 1. Right click in the Project Explorer and select New | Project... | General | Project. Then select Next.
 1. Give the project a name and click the finish button.  This will create a shell project.
 1. Create the basic maven directory structure:
  a. src/main/java - this is the root directory for your main application
  a. src/main/resources - this stores all non-java files that eventually end up on the class path with the java code. 
  a. src/test/java - this is the root directory for your test code.  This code is not included in the final build.

## Create the POM file

On the root directory of your project, create a pom.xml file with the following sample content:

```xml 

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>simoncomputing.app</groupId>
	<artifactId>sample</artifactId>
	<packaging>jar</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<name>Sample Maven Application</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

	<dependencies>
                <dependency>
                        <groupId>junit</groupId>
                        <artifactId>junit</artifactId>
                        <version>4.8.1</version>
                        <scope>test</scope>
                </dependency>

	
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>

		</plugins>
	</build>

</project>


```

 * **&lt;groupId&gt;** - Specifies an ID for the group that this project falls under.  We typically use the package parent name for the project.  For applications at our company, it is simoncomputing.app and for libraries it would be simoncomputing.lib.

 * **&lt;artifactId&gt;** - The project name for the application or library being built.  The standard for our company is lower case and words separated by hyphens.  

 * **&lt;packaging&gt;** - set to **war** for web applications and **jar** for library projects

 * **&lt;properties&gt;** - you can change the value of any properties here.  We set the encoding here to UTF-8 which ensures the build will work in all environments.å


 
## Configure Application for Maven

 * Right click on the project and select Configure | Convert to Maven Project

This will cause Eclipse to read the pom.xml and bring in all of the dependencies.  It will also recognize src/main/java, src/main/resources and src/test/java as the root directory for your source files.

## Test Your Application

 1. Create the home package for your application.  Right click on "src/main/java" and select **New** | **Package** and enter **simoncomputing.app.myapp**.
 1. Create a new class for your application.  Right click on the newly created package: **simoncomputing.app.myapp** and select **New** | **Class**.
 1. Enter your class name and click on [Finish]
 1. Create a simple "Hello World" application.
 1. To run click on the edit window with your new class and select **Run as** | **Java Application**

## Sample HelloApp.java

```java
package simoncomputing.app.myapp;

public class HelloApp {
	public static void main( String[] args ) {
		System.out.println( "Hello!" );
	}

}
```
