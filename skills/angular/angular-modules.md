# Creating Angular Modules

[Angular Tutorials](./README.md)<br>
[Installing Angular Material](./angular-material.md)

## Overview
*Est. Time: 5 minutes*

An Angular Module groups related components, directives and pipes together and
allows them to be shared across other parts of the application. Angular CLI
initializes the application with a root application module at
src/app/app.module.ts.

You could have every component, service, etc. defined in the main application
module, but as your project gets bigger, you'll want to break it up into
separate modules. Typically you have shared modules that have things that are
shared across the entire application and feature modules that are things that
are specific to a group of features that generally are used together.

As a convention, you can create a subdirectory for each module under src/app.

## Prerequisite

 1. You should have done all the tutorials leading up to this page starting from the [Angular Project Setup](./angular-project-setup.md)
 1. Alternatively you can get a copy of the project at: The code is available at: git@gitlab.com:simoncomputing-public/my-app.git
and the code you need to start with is tagged: **angular-material**.  See [Starter Code](#starter-code) below.

 1. Also commit your changes so you can observe what changes when you
    generate a module.

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **angular-material**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout angular-material
yarn
```

## Create a Module

Change directory into the src/app directory and enter the `ng generate` command
for modules. You can shorten `ng -generate` to `ng g`. In the following example,
we can create a module to carry shared tags.

```shell
# assuming you are at the root of your application
# switch to src/app
cd src/app
pwd

# Create the tags module
ng g module tags
```

This reports:
```shell
installing module
  create src/app/tags/tags.module.ts
```

This results in an empty module definition:
```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class TagsModule { }
```
**Note:** This avoids a common beginner mistake of importing BrowserModule
into a sub-module.  That module should only be included on the root module,
all sub-modules should bring in CommonModule instead.

## Bring TagsModule into AppModule

Add the following import to **app.module.ts**.
```typescript
import { TagsModule } from './tags/tags.module';
```

Add add **TagsModule** to the **imports** array property in the
`@NgModule` annotation:
```typescript
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TagsModule,               // add it here
    AppRoutingModule,         // this must be last!
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

### Tag Module Contents

| Property | Content |
| -------- | ------- |
| **declarations** | This contains the list of components, directives, and pipes that belong to this module.  Most of the time you are declaring components and making them available to other components within this module via selector tag. |
| **imports** | Modules that are imported into this module.  Whatever is exported out of these modules are available to everything that is listed in **declarations**. |
| **exports** | List of components, directives, and pipes that is accessible to this module and importers of this module. |
| **providers** | List of DI providers that are accessible inside this module and to importers of this module. |
| **bootstrap** | Components to start application with, usually just AppComponent. |

## Wrap Up

Components you export out of TagsModule will be available at **app.module.ts**.
We'll now add a *component* to this module and export it to show how this works.
Commit your changes before proceeding.


## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **tags-module**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout tags-module
```

**Next Tutorial:**  [Angular Components](./angular-components.md)

