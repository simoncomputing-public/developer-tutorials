# Built-in Directives

[Angular Tutorials](./README.md)<br>
[Template Syntax](./angular-template.md)

## Overview


Directives gives you a way to extend the HTML with custom behavior.  There are three types of directives:

- **Component Directives** are the most common and allow you to create new
tags that define specific parts of web pages.

- **Attribute Directives** allow you to add attributes to tags in the HTML 
and provide custom appearance and/or behavior. 

- **Structural Directives** change the layout of the DOM and arre signified 
by an asterisk in front of the directive atribute name.   

There are two built-in structural directives you will use the most: ```*ngIf``` and ```*ngFor```.  That is what we'll discuss in this tutorial.

## *ngIf

```*ngIf``` allows you to selectively include or exclude portions of the 
HTML based on the truthiness of a value passed to it.

**Syntax:**
```html
<div *ngIf="userId"> 
    <p>User ID: {{userId}}</p>
</div>
```

Try this in a template where the component does not have a **userId** field, and then assign the a 
value to **userId** to see the content reveal itself in the browser. 


## *ngFor

```*ngFor``` allows you to repeat a tag and the HTML content it contains.   The tag is repeated 
for each entry of a list you specify.   You typically use the contents of the list to customize 
the content.  

#### Example 1

Let's do a simple example with a list of strings.  Create an array of strings as shown below
in your component: 

```typescript
private colors = [ "red", "green", "blue" ];
```

In your HTML template, add the following code: 
```html
<select>
  <option *ngFor="let color of colors">{{color}}</option>
</select>
```
The expression ```let color of colors``` causes the ```<option>``` tag to be repeated 
for each entry that is in the ```colors``` array.   The variable: ```color``` is assigned 
the value of each entry and can be used in the tags. 

#### Example 2

This is an example of using objects in the list.  Add an array to the component:
```typescript
private states = [ 
    { code: "VA", description: "Virginia" },
    { code: "DC", description: "Washington D.C." },
    { code: "MD", description: "Maryland" }
];
```

In your HTML template, add the following code: 
```html
<select class="form-control">
    <option *ngFor="let state of states" [attr.value]="state.code">{{state.description}}</option>
</select>
```

**Note** that the input binding syntax for ```attr.value``` evaluates the expression and doesn't need 
the string interpolation syntax.   

**Next Tutorial:** [Reactive Forms](./angular-reactive-forms.md)

## Reference

* https://angular.io/docs/ts/latest/guide/structural-directives.html

