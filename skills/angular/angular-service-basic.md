# Basic Angular Services

[Angular Tutorials](./README.md)<br>
[Angular Components](./angular-components.md)

## Prerequisite
*Est. Time: Jasmine(1 hr), JavaScript Array Functions(1 hr)*

In addition to having completed the Angular tutorials in sequence up until
this tutorial, you'll want to make sure you understand [Jasmine](../skills/jasmine.md).

Building tests will be an integral part of creating the service.

Also get familiar with [JavaScript array functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array):
find(), findIndex() and map().

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **base-components**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout base-components
yarn
```

## Overview
* Est. Time: 45 minutes
*
"Service" is a heavily used word in software development and in Angular,
"service" has a specific meaning separate and apart from services such as
Web Services and RESTful services.

**In Angular, a services is a singleton object** that is available to the running
instance of your application.

Any shared functionality that you want to be available across the entire application
can be placed in a service.  It is typical to have shared data stored in a service
such as look up tables.

The other common use for services is to **call** RESTful services.  In that case
the Angular service is a "client" to the REST service.  These services
introduce another layer of complexity with timing and handling communication errors.
We'll take that on in a later tutorial.

For this tutorial, we will create a simple service that maintains a shared in-memory
repository of users.

## Create a User Module

Let's start by creating a module to contain the user service and related classes.

```shell
cd src/app
ng g module user-service
```

Add the new UserServiceModule to **app.module.ts** by importing it and then putting
it on the list of imports as shown below:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TagsModule } from './tags/tags.module';

// <-- Add this line
import { UserServiceModule } from './user-service/user-service.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TagsModule,
    UserServiceModule,                                         // <-- Add this line
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

 >**Tip**: If you are using [Visual Studio Code](../env-setup/visual-studio-code.md), you can go straight to the `imports`
 and type the module name, then press ⌘ + ".", then arrow down to select "Import UserServiceModlue from 'app/user-service/user-service.module'".
 This automatically adds the import so you don't have to worry about typos in the import statement.

---
## Setup the Data Model

It's helpful to layout what the data will look like ahead of time.   We'll
define a couple of classes here to describe a User.

Create the following under /src/apps/user-service/user.ts

```typescript
export class User {
  userId = '';
  role = '';          // radio buttons - Admin, PowerUser, User, Guest
  lastName = '';
  firstName = '';
  address: Address;
  phones: Phone[];
  title = '';         // select box: 'Project Manager', 'Analyst', 'Technical Lead',
                      // 'Developer', 'DevOps', 'Tester'

  active = false;     // Checkbox
}

export class Address {
  street = '';
  city   = '';
  state  = '';
  zip    = '';
}

export class Phone {
  type = '';          // home, work, cell
  phoneNo = '';
}

// This contains some short lists to support entry screens
export const states = [ 'VA', 'FL', 'CA', 'NY', 'MD', 'DC' ];
export const titles = [ 'Project Manager', 'Analyst', 'Technical Lead',
                        'Developer', 'DevOps', 'Tester' ];
export const roles  = [ 'Admin', 'PowerUser', 'User', 'Guest'];
```
The above is a simplistic view of a User class and it's associated classes.

When we build the service, we'll manage a list of users.  When an application requests
a user from the list, we should give it a copy so that it cannot directly change the
contents.  If we want to do this, creating [copy constructors](https://stackoverflow.com/questions/29362169/why-do-we-need-copy-constructor-and-when-should-we-use-copy-constructor-in-java)
 will help that process.

Update **user.ts** with the following:
```typescript


export class User {
  userId = '';
  role? = '';          // radio buttons - Admin, PowerUser, User, Guest
  lastName = '';
  firstName = '';
  address: Address;
  phones: Phone[];
  title? = '';         // select box: 'Project Manager', 'Analyst', 'Technical Lead',
                      // 'Developer', 'DevOps', 'Tester'

  active = false;     // Checkbox

  constructor( that?: User ) {
    if ( !that ) return;

    this.userId    = that.userId;
    this.role      = that.role;
    this.lastName  = that.lastName;
    this.firstName = that.firstName;

    if ( that.address )
      this.address = new Address( that.address );

    if ( that.phones )
      this.phones = that.phones.map( phone => new Phone( phone ));

  }
}

export class Address {
  street = '';
  city   = '';
  state  = '';
  zip    = '';

  // copy constructor
  constructor( that?: Address ) {
    if ( !that ) return;

    this.street = that.street;
    this.city    = that.city;
    this.state   = that.state;
    this.zip     = that.zip;
  }
}

export class Phone {
  type = '';          // home, work, cell
  phoneNo = '';

  // copy constructor
  constructor( that?: Phone ) {
    if ( !that ) return;

    this.type    = that.type;
    this.phoneNo = that.phoneNo;
  }
}

// This contains some short lists to support entry screens
export const states = [ 'VA', 'FL', 'CA', 'NY', 'MD', 'DC' ];
export const titles = [ 'Project Manager', 'Analyst', 'Technical Lead',
                        'Developer', 'DevOps', 'Tester' ];
export const roles  = [ 'Admin', 'PowerUser', 'User', 'Guest'];
```

### Don't forget the unit tests

The following is a partially completed user test.  It will highlight mistakes in the code that you should proceed to fix.  You can disable tests by prefixing ```it()``` with **x** as in ```xit()```.   You can also focus your test on a single test by prefixing ```it()``` with **f** as in ```fit()```.

If you are relatively new to TypeScript and Jasmine Testing, you should go through this and try to figure out the problems.  This will give you important insight into what you are doing.

If you don't have time, the corrected code can be found at **git@gitlab.com:simoncomputing-public/my-app.git** on the **user-test** tag.

```typescript
// user.spec.ts
import { User, Phone, Address } from "app/user-service/user";

interface ThisContext {
    user: User,
    address: Address,
    phones: Phone[]
}

describe("User Model", function(){

    beforeEach(function(this: ThisContext){
        this.user = new User();
        this.user.userId = 'one';
        this.user.role = 'rolo';
        this.user.lastName = 'lname';
        this.user.firstName = 'fname';
        this.user.title = 'titulo';
        this.user.active = true;

        this.address = new Address();
        this.address.street = "123 Main Street";
        this.address.city = "Anytown";
        this.address.state = "IL";
        this.address.zip = "12345";

        this.phones = [
            new Phone(),
            new Phone()
        ];
        this.phones[0].type = "home";
        this.phones[0].phoneNo = "123-456-7890";
        this.phones[1].type = "cell";
        this.phones[1].phoneNo = "890-567-1234";
    });

    describe("Create User", function(){
        it("when argument is null", function(){
            // act
            let result = new User();

            // assert
            expect(result).toBeTruthy();
            expect(result.userId).toBe('');
            expect(result.role).toBe('');
            expect(result.lastName).toBe('');
            expect(result.firstName).toBe('');
            expect(result.title).toBe('');
            expect(result.active).toBe(false);
            expect(result.phones).toBeFalsy();
            expect(result.address).toBeFalsy();
        });

        describe("when argument is not null", function(){

            it("has no children", function(this: ThisContext){
                // act
                let result = new User(this.user);
                this.user.userId = 'two'; // make an edit

                // assert
                expect(result).toBeTruthy();
                expect(result.userId).toBe('one');
                expect(result.role).toBe('rolo');
                expect(result.lastName).toBe('lname');
                expect(result.firstName).toBe('fname');
                expect(result.title).toBe('titulo');
                expect(result.active).toBe(true);
                expect(result.phones).toBeFalsy();
                expect(result.address).toBeFalsy();
            });

            it("has address", function(this: ThisContext){
                // arrange
                this.user.address = this.address;

                // act
                let result = new User(this.user);
                this.user.address.state = 'VA'; // make an edit to the original;

                // assert
                expect(result.address).toBeTruthy();
                expect(result.address.street).toBe('123 Main Street');
                expect(result.address.city).toBe('Anytown');
                expect(result.address.state).toBe('IL');
                expect(result.address.zip).toBe('12345');
            });

            it("has phones", function(this: ThisContext){
                // arrange
                this.user.phones = this.phones;

                // act
                let result = new User(this.user);
                this.user.phones.push(new Phone()); // make an edit to the original;

                // assert
                expect(result.phones).toBeTruthy();
                expect(result.phones.length).toBe(2);
                expect(result.phones[0]).toEqual(this.phones[0]);
                expect(result.phones[1]).toEqual(this.phones[1]);
            });
        });
    });
});
```

---
## Create the Service

Generate a service in the user-service module.

```shell
cd src/app/user-service
pwd
ng g service user
```

Note that `.service` was appended to the filenames:

```shell
installing service
  create src/app/user-service/user.service.spec.ts
  create src/app/user-service/user.service.ts
  WARNING Service is generated but not provided, it must be provided to be used
```
The warning says that the service is not provided anywhere. We will fix that now
by adding the user service as a provider to the  **user-service.module.ts**.

You'll need to:
 1. Import **UserService**
 2. Put user on the **providers** list.

```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './user.service';        // <-- Add this line

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[UserService],                          // <-- Add this line
  declarations: []
})
export class UserModule { }
```
## Creating Service Functionality

Now that we've created the shell for the UserService, we need to give it functionality.
We want to be able to:

1. Add a user
1. Get a user
1. Update a user
1. List all users
1. Delete a user

Let's start by updating the UserService class with a basic interface for these
functions:

1) Import the User, Address and Phone classes as well as the error classes.

```typescript
import { User, Address, Phone } from './user';
```

2) Create an empty user list in the class:
```typescript
  private userList : User[];
```

3) Create the stub function calls.  We'll use the convention of returning an
error message string if there is an error, and null if no errors occurred.

**Challenge:** Create the stub functions without looking at the example shown below first.

The entire class should look like this:
```typescript
import { Injectable } from '@angular/core';
import { User, Address, Phone } from './user';

@Injectable()
export class UserService {
  private userList : User[];

  constructor() { }

  /**
   * Adds the specified user to the userList.  If there are any errors,
   * it returns an error message.  Otherwise, null is returned.
   * @param user
   */
  addUser( user: User ) : string {
    return null;
  }

  /**
   * Returns the specified user.  Returns null if not found.
   *
   * @param userId
   */
  getUser( userId: string ) : User {
    return null;
  }

  /**
   * Updates the existing user entry with the provided user entry.
   * Returns null if there are no errors.
   *
   * @param user
   */
  updateUser( user: User ) : string {
    return null;
  }

  /**
   * Deletes the specified user.  It returns an error message
   * if the user is not found.
   *
   * @param userId
   */
  deleteUser( userId ) : string {
    return null;
  }

  /**
   * Returns the full user list.
   */
  getUsers() : User[] {
    return null;
  }

}
```
Now we have stubs for everything.

To prepare for the next section, go to your application's root directory and
start the automated tests:

```shell
ng test
```
You should see all generated tests pass.

This runs an initial test and you should see that all generated tests pass.

This test runner continues to run every time the source code changes.
As you code, make sure you fix the code or test to resolve any testing errors.


## Creating the Test Stubs

When you first created the service, a **user-service.spec.ts** file was created with a
starter set of tests:

```typescript
import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from "app/user-service/user";

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService]
    });
  });

  it('should ...', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
```

We're going to modify this file to have an entry for each service call we created:

```typescript
// (remove the import of `TestBed` and `inject` which is overkill for testing a service

import { UserService } from './user.service';
import { User } from "app/user-service/user";

// add this interface
interface ThisContext {
  user: User,
  service: UserService,
}

describe('UserService', function() {         // <-- change from fat arrow!
  beforeEach( function(this: ThisContext) {                 // <-- change from fat arrow!
    this.service = new UserService();                       // <-- use this instead of `TestBed` and `inject`
  });

  it('Should create service', function(this: ThisContext) { // <-- get rid of the inject wrapper function
    expect(this.service).toBeTruthy();
  });

  it('Should add user', function(this: ThisContext) {
    pending();
  });

  it('Should get user', function(this: ThisContext){
    pending();
  });

  it('Should update user', function(this: ThisContext){
    pending();
  });

  it('Should delete user', function(this: ThisContext){
    pending();
  });

  it('Should get the user list', function(this: ThisContext){
    pending();
  });

});
```
Note that we got rid of the fat arrow notation and replaced it with the standard
anonymous functions.  This provides a very useful behavior where you can assign things
to ```this``` in the ```beforeEach()``` function.  All the test will start with a new
```this``` context with whatever you assign to ```this``` in the ```beforeEach()```
function.

## Writing the addUser() implementation

When writing the implementation of the function not only do you want to perform a specific
action, you also want to consider all the things that could go wrong.  For
addUser( user ), what are the things that could go wrong?

1. addUser() could be called with a null parameter.
1. addUser() could be called with a userId that already exists.
1. addUser() could be called with a null userId, which would make it impossible to retrieve
   later.

Let's look at an implementation of addUser() that would take all of this into consideration:

```typescript
  /**
   * Adds the specified user to the userList.  If there are any errors,
   * a message is returned.  Otherwise, null is returned.
   * @param user
   */
  addUser( user: User ) : string {
    if ( !user ) return 'user is required.';
    if ( !user.userId ) return 'userId is required.';

    // check for duplicate
    if ( this.getUser( user.userId ) )
      return `${user.userId} already exists.`;

    this.userList.push( user );

    return null;
  }
```
This function checks for a null user, null userId and duplicate userId before it
will add the user to the list.

in the **user.service.spec.ts** file, we can write tests to make sure that both the
happy path and error paths are all being tested.   In our test, it will be handy to
have a pre-built User object for testing in multiple tests.  To write the code once,
put it in the the **beforeEach()** call like this:

```typescript
  beforeEach( function(){
    this.service = new UserService(this.mockHttp);

    this.user = new User();
    this.user.userId = 'superman';
    this.user.firstName = 'Clark';
    this.user.lastName  = 'Kent';
    this.user.role      = 'PowerUser';
    this.user.title     = 'Project Manager';

  });
```

Note that we create a user and assign it to ```this```.   Every test that a test is run
it is initialized with a new ```this``` context and ```this.user``` is recreated.  If
you added anything to the ```this``` context in one test, it is cleared out before the
next test is run.   This is a handy way to initialize each test.  You must change the
fat arrow notation to a traditional function declaration for this to work.

Now let's write the test for addUser():
```typescript
describe('UserService', function() {
  // ...

  // convert original `it` to `describe` so we can have separate cases
  describe('should add user', function(){
    it("when user is null", function(this: ThisContext){
      // act
      let result = this.service.addUser(null);

      // assert
      expect(result).toBeTruthy();
    });

    it("when userId is null", function(this: ThisContext){
      // arrange
      this.user.userId = null;

      // act
      let result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
    });

    it("when all is good", function(this: ThisContext){
      // arrange
      let userId = this.user.userId;

      // act
      let result = this.service.addUser(this.user);
      let retrieved = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(retrieved).toBeTruthy();
      expect(retrieved.userId).toEqual(userId);
      expect(retrieved.firstName).toEqual(this.user.firstName);
      expect(retrieved.lastName).toEqual(this.user.lastName);
      expect(retrieved.role).toEqual(this.user.role);
      expect(retrieved.title).toEqual(this.user.title);
    });

    it("when already exists, fails", function(this: ThisContext){
      // arrange
      let userId = this.user.userId;
      this.service.addUser(this.user);

      // pre-condition assert
      expect(this.service.getUser(userId)).toBeTruthy();

      // act
      let result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
      expect(result).toContain("superman already exists");
    });
  });

  // ...
}

```
Note that the above tests all the potential error conditions we discussed as well as
the happy path. We also broke out the tests into each relevant test case, and nested them inside another `describe`.

Now go through and implement the rest of the functions and tests.  A working version is
provided below but please try to do it on your own before looking at the example.

## user.service.ts

```typescript
import { Injectable } from '@angular/core';
import { User, Address, Phone } from './user';

@Injectable()
export class UserService {
  private userList: User[] = [];

  constructor() { }

  /**
   * Adds the specified user to the userList.  If there are any errors,
   * a message is returned.  Otherwise, null is returned.
   * @param user
   */
  addUser( user: User ): string {
    if ( !user ) return 'user is required.';
    if ( !user.userId ) return 'userId is required.';

    // check for duplicate
    if ( this.getUser( user.userId ) )
      return `${user.userId} already exists.`;

    this.userList.push( user );

    return null;
  }

    /**
   * Returns a copy of the specified user.  Returns null if not found.
   *
   * @param userId
   */
  getUser( userId: string ): User {
    const user = this.userList.find( currentUser => currentUser.userId === userId );
    if ( !user ) return null;

    return new User( user );
  }

  /**
   * Updates the existing user entry with the provided user entry.
   * Returns null if there are no errors.
   *
   * @param user
   */
  updateUser( user: User ): string {
    if ( user == null ) return 'null user not allowed.';

    const index = this.userList.findIndex( u => u.userId === user.userId );
    if ( index < 0 ) return `user.userId[${user.userId}] doesn't exist`;

    this.userList[index] = user;
    return null;
  }

  /**
   * Deletes the specified user.  It returns EntryError
   * if not specified.
   *
   * @param userId
   */
  deleteUser( userId ): string {
    if ( !userId ) return 'Valid userId required';

    const index = this.userList.findIndex( u => u.userId === userId );
    if ( index < 0 ) return `user.userId[${userId}] doesn't exist`;

    this.userList.splice( index, 1 );
    return null;
  }

  /**
   * Returns the full user list.
   */
  getUsers(): User[] {
    return this.userList.map( u => new User( u ) );
  }


}

```

## user.service.spec.ts
```typescript
import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from './user';


// add this interface
interface ThisContext {
  user: User;
  service: UserService;
}

describe('UserService', function() {
  beforeEach( function( this: ThisContext ){
    this.service = new UserService();

    this.user = new User();
    this.user.userId = 'superman';
    this.user.firstName = 'Clark';
    this.user.lastName  = 'Kent';
    this.user.role      = 'PowerUser';
    this.user.title     = 'Project Manager';


  });

  describe('should add user', function(){
    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.addUser(null);

      // assert
      expect(result).toBeTruthy();
    });

    it('when userId is null', function(this: ThisContext){
      // arrange
      this.user.userId = null;

      // act
      const result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
    });

    it('when all is good', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;

      // act
      const result = this.service.addUser(this.user);
      const retrieved = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(retrieved).toBeTruthy();
      expect(retrieved.userId).toEqual(userId);
      expect(retrieved.firstName).toEqual(this.user.firstName);
      expect(retrieved.lastName).toEqual(this.user.lastName);
      expect(retrieved.role).toEqual(this.user.role);
      expect(retrieved.title).toEqual(this.user.title);
    });

    it('when already exists, fails', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // pre-condition assert
      expect(this.service.getUser(userId)).toBeTruthy();

      // act
      const result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
      expect(result).toContain('superman already exists');
    });
  });

  describe('should get user', function(){
    it('when user exists', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // act
      const result = this.service.getUser(userId);

      // assert
      expect(result).toEqual(this.user);
    });

    it('when user does not exist', function(this: ThisContext){

      // act
      const result = this.service.getUser('something non-existent');

      // assert
      expect(result).toBeNull();
    });

    it('safe copy', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // act
      const result = this.service.getUser(userId);

      // assert
      expect(result).toEqual(this.user);
      expect(result).not.toBe(this.user);
    });
  });

  describe('should update user', function(){

    it('when fields updated', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      const user = this.service.getUser(userId);

      // act
      user.firstName = 'updated ' + this.user.firstName;
      user.lastName = 'updated ' + this.user.lastName;
      user.role = 'updated ' + this.user.role;
      user.title = 'updated ' + this.user.title;
      const result = this.service.updateUser(user);
      const updatedUser = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(updatedUser).toBeTruthy();
      expect(updatedUser.firstName).toEqual(user.firstName);
      expect(updatedUser.lastName).toEqual(user.lastName);
      expect(updatedUser.role).toEqual(user.role);
      expect(updatedUser.title).toEqual(user.title);
    });

    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.updateUser(null);

      // assert
      expect(result).toBeTruthy();
    });

  });

  describe('should delete user', function(){

    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.deleteUser(null);

      // assert
      expect(result).toBeTruthy();
    });

    it('when user exists', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);
      // pre-condition assert
      expect(this.service.getUser(userId)).toEqual(this.user);

      // act
      const result = this.service.deleteUser(userId);
      const exists = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(exists).toBeNull();
    });

    it('when user does not exist', function(this: ThisContext){

      // act
      const result = this.service.deleteUser('some non-existent');

      // assert
      expect(result).toBeTruthy();
    });
  });

  describe('should get all users', function(){

    it('when only one user', function(this: ThisContext){
      // arrange
      this.service.addUser(this.user);

      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(this.user);
    });

    it('when no users', function(this: ThisContext){
      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(0);
    });

    it('when many users', function(this: ThisContext){
      // arrange
      for (let i = 0; i < 5; i++) {
        const user = new User(this.user);
        user.userId = user.userId + i;
        this.service.addUser(user);
      }

      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(5);
    });
  });



});

```
## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **user-service**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout user-service
```


**Next Tutorial:** [Template Syntax](./angular-template.md)
