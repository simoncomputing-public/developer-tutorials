# Angular Routing - Dependency Injection

[Angular Tutorials](./README.md)<br>
[Routing - Setting up the Routing Table](./angular-routing-1.md)

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **routing-1**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout routing-1
yarn
```

## Overview

You can create navigation links with either an anchor tag (```<a>```) or a button tag:

```html
<a routerLink="/add-user">Add User</a>
<button routerLink="/add-user">Add User</a>
```
This will give us the basic tools to provide navigation.  Let's start building out the functionality
for adding and managing user entries and start tying these pages together.

## Understanding Providers

We built the **UserService** in the [Angular Services - Basic](./AngularServiceBasic.md) tutorial.
When we put it on the **providers** list in **user-service.module.ts** like so:

```typescript
  providers:[UserService]
```

It was a short hand version of adding this:

```typescript
  providers: [ {provider: "UserService", useClass: "UserService" } ]
```
The **provider** is a key and it happens to be the class **User Service**.   The **useClass** is the
class used to instantiate the object that get's returned.  The object is instantiated only once so
the class is essentially a singleton.

Since most of the time, both value are the same, the short hand version is sufficient.

**Note:** Keep it simple.  Services are intended to be singletons that are globally available to
the entire application.  For that reason, only provide them from a single module in the application,
either in the root module or a module imported by the root module.  In both cases, your service will
become globally accessible.

## Understanding Constructor Dependency Injection

The Constructor Dependency Injection syntax allows you to ask for a specific ```@Injectable``` object such as services and components.   To gain access to the UserService in your Component class, create a constructor with a UserService parameter.

Let's do that now to **userlist.component.ts**.  First import the User and UserService:

```typescript
import { User } from '../../user-service/user';
import { UserService } from '../../user-service/user.service';
```

Then add this constructor which causes UserService to be injected into the class:
```typescript
  constructor( private service: UserService ) { }
```
Now ```this.service``` is available to be used by the component.

Let's make the component load the users on init:
```typescript
import { Component, OnInit } from '@angular/core';
import { User } from '../../user-service/user';
import { UserService } from '../../user-service/user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {

  users: User[]; // <-- add this

  constructor(private service: UserService) { }
  ngOnInit() {
    this.users = this.service.getUsers(); // <-- add this
  }

}

```

Go to **userlist.component.html** and let's put some debugging code in it:
```typescript
<mat-card>

  <h1>User List</h1>
  <p>Users: {{service.getUsers() | json }}</p>


</mat-card>
```

When you go to http://localhost:4200/userlist you'll see that it displays an empty array.  We need to
add users so add the following button to the bottom of **userlist.component.html** after the
```<h1>User List</h1>``` line.

```typescript
        <button mat-raised-button color="primary" routerLink="/add-user">Add User</button>
```

The routing will not work yet until you import the **RouterModule** to **user.module.ts**
```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserlistComponent } from './userlist/userlist.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';          // <-- Add this line


@NgModule({
  imports: [
    CommonModule, MaterialModule, RouterModule, ReactiveFormsModule
  ],
  exports: [UserComponent, UserlistComponent],
  declarations: [UserComponent, UserlistComponent]
})
export class UserModule { }
```

Test the **Add User** button and it should now navigate you to the Add User page.

## Saving a User

The next step is to inject UserService to the UserComponent, add a function to save, and then
add a button to call the save function.

In **user.component.ts**:

1. Add ```error``` to contain error messages.
1. Add the **UserService** to the constructor.
1. Create and **addUser()** function.

```typescript
 error = null;

 constructor(private fb: FormBuilder,
              private service: UserService ) {
  }

  addUser() {
    this.error = this.service.addUser( this.userForm.value );
  }
```

In **user.component.html**:

1. Add a save button that will call the addUser() function.
1. Add some debugging text to show the error and also the list of services.

```html
          <button mat-raised-button color="primary" (click)="addUser()">Add</button>

          <p>Users: {{service.getUsers() | json }}</p>
          <p>Error: {{error}}</p>
```

Now you can test the form by going to http://localhost:4200/add-user.  Try the following:

1. Click on add without entering anything.  It will return an error.
1. Add the first three fields and it will add successfully.  You'll see your user added to the list.
1. Click on it again and it will complain that this is a duplicate.
1. Change the user ID and it will add a second entry to the list.

**Optional Challenge:** The validation for last name was accidentally omitted.  Add the validation at the service layer.

#### A friendlier Add Button

We should disable the Add button when the form is invalid.

On the form, the **Form status** is INVALID until the userId and lastName fields that are specified as required in the HTML template have values in it.  This value comes from ```userForm.status```.
The button can be disabled by adding **disabled** to the class.  This can be done by using
the template syntax set the class based on the state of ```userForm.status```.
```html
      <button mat-raised-button
              color="primary"
              [disabled]="userForm.status === 'INVALID'"
              (click)="addUser()">Add</button>
```

#### Making the error message standout

In the **styles.scss** file, let's add the following Bootstrap goodies so we can make the UI a little nicer:

```css
.hidden {
    visibility: hidden
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
  }

.alert-danger {
    color: #721c24;
    background-color: #f8d7da;
    border-color: #f5c6cb;
}

.alert-primary {
    color: #004085;
    background-color: #cce5ff;
    border-color: #b8daff;
  }

  .alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
  }

  .alert-warning {
    color: #856404;
    background-color: #fff3cd;
    border-color: #ffeeba;
  }


```

We also set the entire div to be hidden if there is no error.  Place the following HTML below the button:
```html
          <br>
          <br>
          <div [class.hidden]="!error" class="alert alert-danger" role="alert">
            <strong>Oh snap!</strong> {{error}}
          </div>
```

## Navigating by Button

When a user cancels entry, we want to take them directly back to the User List.   We can do this simply by adding a Cancel button link right after the Add button.
```html
      <button mat-raised-button
              routerLink="/userlist">Cancel</button>
```

When we add, we can return the User List page so that they can see the result of adding a User there.
Because we have to add the user first, we can't just put a link on the page.  We have to do this
programatically.

To do this we will inject the ```Router``` into **user.component.ts**:
```typescript
import { Router } from "@angular/router";
...
...
...

  constructor(private fb: FormBuilder,
              private service: UserService,
              private router: Router  ) {
  }

```

And then in the addUser() function,

```typescript
  addUser() {
    this.error = this.service.addUser( this.userForm.value );

    if ( this.error ) {
      return;
    }

    this.router.navigate( ['/userlist'] );
  }
```
Note that ```navigate()``` takes an array.  The first field is the route and the following parameters
are added if you have parameters.  For now, let's just navigate to userlist.

One last thing we can do is make the error message disappear after about 5 seconds.   This is an
example of having a timed event in Angular.  For this to work, we will setup a function specifically
to handle clear the delay and call it right after you set the error:
```typescript
import { Observable } from 'rxjs/Rx';
...
...
...
  delayedClear() {
    let timer = Observable.timer( 5000 );
    timer.subscribe( x => this.error = null );
  }

  addUser() {
    this.error = this.service.addUser( this.userForm.value );

    if ( this.error ) {
      this.delayedClear();                      // <-- Add this call
      return;
    }

    this.router.navigate( ['userlist'] );
  }
```

#### Making the error message standout

In the **styles.scss** file, let's add the following Bootstrap goodies so we can make the UI a little nicer:

```css
.hidden {
    visibility: hidden
}

.alert {
    padding: 0.75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
  }

.alert-danger {
    color: #721c24;
    background-color: #f8d7da;
    border-color: #f5c6cb;
}

.alert-primary {
    color: #004085;
    background-color: #cce5ff;
    border-color: #b8daff;
  }

  .alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
  }

  .alert-warning {
    color: #856404;
    background-color: #fff3cd;
    border-color: #ffeeba;
  }


```
We also set the entire div to be hidden if there is no error.  Place the following HTML below the button:
```html
          <br>
          <br>
          <div [class.hidden]="!error" class="alert alert-danger" role="alert">
            <strong>Oh snap!</strong> {{error}}
          </div>
```

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **routing-2**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout routing-2
```

**Next Tutorial:** [Routing - Part 3](./angular-routing-3.md)


## References

* https://angular.io/docs/ts/latest/guide/dependency-injection.html
* https://material.angular.io/components/button/overview
