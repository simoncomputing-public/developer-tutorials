# Installing Angular Material

[Angular Tutorials](./README.md)<br>
[Angular Project Setup](./angular-project-setup.md)

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **base-setup**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout base-setup
yarn
```

## Overview
*Est. Time: 7 minutes*

This is the Angular Material Library

**1)** Install Angular Material

```shell
npm install --save web-animations-js @angular/animations @angular/material @angular/cdk hammerjs
```

Uncomment **web-animation-js** from  **polyfill.ts**:

```typescript
/** IE10 and IE11 requires the following to support `@angular/animation`. */
import 'web-animations-js';  // Run `npm install --save web-animations-js`.
```

**2** Create an MaterialModule to contain all Angular imports

The materials module allows you to selectively include the components that
you'll use on the project.   The following contains the components that you'll
use for this tutorial.

```shell
cd src/app
pwd

ng g module material

```

Update **material.module.ts** with the following content:

```typescript
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatGridListModule,
  MatSelectModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSnackBarModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule],
  exports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule],
  declarations: []
})
export class MaterialModule { }


```

**3)** Add a theme

Add the following line to **styles.scss**:

```css
@import "~@angular/material/prebuilt-themes/indigo-pink.css";
.fill-space {
    flex: 1 1 auto;
}
body {
    margin: 0px;
}
```

**4)** Add Gesture Support

Import into **main.ts**:
```typescript
import 'hammerjs';
```

**5)** Add Material Icons

Add the following into **index.html**:

```html
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **angular-material**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout angular-material
```

**Next Tutorial:** [Angular Modules](./angular-modules.md)

## Reference

* https://material.angular.io/guide/getting-started

