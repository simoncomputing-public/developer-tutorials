# Angular Routing - Setting up the Routing Table

[Angular Tutorials](./README.md)<br>
[Reactive Forms](./angular-reactive-forms.md)

## Prerequisite

Now that you've covered:

3. [Modules](./angular-modules.md)
4. [Components](./angular-components.md)
4. [Angular Services - Basic](./angular-service-basic.md)
4. [Template Syntax](./angular-template.md)
4. [Structural Directives](./angular-directives.md)
5. [Reactive Forms](./angular-reactive-forms.md)

We are now ready to pull it all together with routing.

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **reactive-forms**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout reactive-forms
yarn
```

## Overview
* Est. Time: 5 minutes*

Routing allows you to create navigation logic from one view to another.

In an earlier tutorial we created a project with routing.  This was done by
adding the `--routing` paramater to the CLI generate command:

```shell
# example (don't run this)
ng new my-app --routing
```

There are a couple of things that were created when this command is run:

 1. In **index.html**, you'll see the following tag: ```<base href="/">```
    This allows you to specify the root context of the application.   This is
    the URL that all other addresses are relative to.

 2. An **app-routing.module.ts** was created.

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

```

## Customizing Routes

Let's establish routes for our two main Component classes: **UserComponent**
and **UserlistComponent**.

In **src/app/app-routing.module.ts** add the following imports:
```typescript
import { UserComponent } from './user/user/user.component';
import { UserlistComponent } from './user/userlist/userlist.component';
```

Replace the existing **Routes** with the ones listed below.

```typescript
const routes: Routes = [
  { path: 'add-user', component: UserComponent },
  { path: 'userlist', component: UserlistComponent },
];
```

In **app.component.html**, remove the hard coded entries that called ```UserComponent``` and ```UserlistComponent``` directly.  It should
look like this:
```html
<app-header></app-header>
<router-outlet></router-outlet>
<app-footer></app-footer>
```

We will now rely on the router to display those pages.

With ```ng serve``` running, you can go directly to each page by entering the
following URL in your browser:


* http://localhost:4200/add-user
* http://localhost:4200/userlist


Note the structure of the URL:
    - **http://localhost:4200** - is the location of your server started by ```ng serve```.
    - **/** - is the root directory specified in your **index.html** with the ```  <base href="/">``` tag.
    - **add-user** - is the path that you specified in the Routes table.


## Setting a Default Path

When someone arrives at your site, if they only specify the root of your application,
the should be redirected to **userlist**.   Do this by adding the **redirectTo** path entry
to the Routes array as shown below:

```typescript
const routes: Routes = [
  { path: 'add-user', component: UserComponent },
  { path: 'userlist', component: UserlistComponent },
  { path: '', redirectTo: 'userlist', pathMatch: 'full' }    //<-- Add this line!
];
```

Confirm this works by going to http://localhost:4200.

## Setting a "Page Not Found" Path

At the end of the Components tutorial, you create a **NotFoundComponent**.   We
can route all requests that do not resolve to a component we have to the
**NotFoundComponent**.

Ideally, you will update this page with something clever to give a user a
friendly response when a "404 Page Not Found" occurs.

1) Import the **NotFoundComponent** into **app-routing.module.ts**:
```typescript
import {NotFoundComponent } from './tags/not-found/not-found.component';
```
2) Indicate a catch all path by adding a ```{path: '**', NotFoundComponent }```
statement as the last entry of the Routes array.  This must be the last
entry since it catches everything.   Anything paths defined below this
will be ignored.

```typescript
const routes: Routes = [
  { path: 'add-user', component: UserComponent },
  { path: 'userlist', component: UserlistComponent },
  { path: '', redirectTo: 'userlist', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }              // <-- Add this line
];
```
You can confirm that this works by going to the browser, entering http://localhost:4200/ and appending junk to the URL like this:

http://localhost:4200/asdfasdf

When the router tries to resolve "asdfasdf", it doesn't match any of the path statements until it reaches the catch-all path of "**".   At that point it will
resolve to the NotFoundComponent.

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **routing-1**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout routing-1
```

**Next Tutorial:** [Routing - Part 2](./angular-routing-2.md)


