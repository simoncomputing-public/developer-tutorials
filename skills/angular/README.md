# Angular Tutorials

[Back to Developer Tutorials](../README.md)

## Overview

*11 Minutes*

This is the point at which you should fire up Visual Studio Code.  It is an 
excellent tool that is similar to Sublime and it is free.  

Note that the tutorial focuses on teaching you the mechanics of using Angular 
first before taking on a lot of unit testing.  Most of the testing tutorials 
are at the end.  

Once you have all of that under your belt, you can do it the right way which 
is testing a lot up front and while you are building the front-end. 

1. [Angular Project Setup](./angular-project-setup.md)
1. [Angular Material Design](./angular-material.md)
1. [Modules](./angular-modules.md)
1. [Components](./angular-components.md)
1. [Angular Services - Basic](./angular-service-basic.md)
1. [Template Syntax](./angular-template.md)
1. [Structural Directives](./angular-directives.md)
1. [Reactive Forms](./angular-reactive-forms.md)
1. Routing
    * [Routing - Part 1](./angular-routing-1.md) - Setup the Routing Table
    * [Routing - Part 2](./angular-routing-2.md)
    * [Routing - Part 3](./angular-routing-3.md)
    * [Writing User Component Tests](./angular-user-component-testing.md)