# Template Syntax

[Angular Tutorials](./README.md)<br>
[Angular Services - Basic](./angular-service-basic.md)

## Overview

Template syntax allows you to embed data from your component class into various
parts of the HTML.  It also allows you to trigger calls to functions in your 
component from Javascript events.   

The following sections will describe the most commonly used features.  Use the
newly created UserlistComponent to try out the various template syntax and 
solidify your understanding.  We will then move on to using these in our sample
application.

## String Interpolation 1: Embed Expressions into HTML 

String interpolation is a form of **input binding**, *it binds data to the view*.  
As the data changes, the view in the browser is updated.

You can embed a string variable or a read-only expression in double brackets
directly into any part of a component's HTML.  Most simple Javascript expressions
can be used as long as they don't have side effects that change the state of
the component.  

Examples of things that won't work are assignments (=, +=, -=, etc.) and increment/decrement operators (++,--).  
There are other things that don't work. Keep it simple and you'll stay out of trouble.

#### Syntax:
```html
<p>{{ myValue }}</p>
<input value="{{ myValue }}">
The count is {{ getCount() }}
The count is {{ getCount() + 2 }}
```
| If defined in component | in HTML template | It is rendered in browser as: | 
| ----- | -------- | -------- |
| ```private myValue="Hello!";``` | ```<h1>{{myValue}}</h1>``` | ```<h1>Hello!</h1>``` |
| ```private myValue="Hello!";``` | ```<input value="{{ myValue }}">``` | ```<input value="Hello!">``` |
| ```getCount() { return 10};``` | ```The count is {{ getCount() }}``` | ```The count is 10``` |
| ```getCount() { return 10};``` | ```The count is {{ getCount() + 2 }}``` | ```The count is 12``` |

## String Interpolation 2: Embed Expressions with Pipes 

Pipes take a value and transform them.  The following example would 
convert the object into a JSON string.

#### Syntax:
```html
{{ myObject | json }}
```
There are multiple built-in pipes.  To look at some examples of how they can
be used, define the following in your component:

```typescript
private myObject = { firstName: "John", lastName: "Doe" };
private myValue = "Hello!";
private today  = new Date(2017, 5, 3);  // June 3, 2017
private format = "MM/dd";
```

| in HTML template | It is rendered in browser as: | 
| -------- | -------- |
| &lt;h1&gt;{{myValue}}&lt;/h1&gt; | ```<h1>Hello!</h1>``` |
| &lt;input value="{{ myValue &#124; uppercase }}"&gt; | ```<input value="HELLO!">``` |
| &lt;input value="{{ myValue &#124; lowercase }}"&gt; | ```<input value="hello!">``` |
|&lt;p&gt;myObject: {{ myObject &#124; json }}&lt;p&gt; | ```<p>myObject: { "firstName": "John", "lastName": "Doe" }</p>``` |

Pipes can accept parameters either as literals or variables.. Here are examples with the [date pipe](https://angular.io/docs/ts/latest/api/common/index/DatePipe-pipe.html).

| in HTML template | It is rendered in browser as: | 
| -------- | -------- |
|&lt;input value="{{ today &#124; date }}"&gt; | ```<input value="Jun 3, 2017">``` |
| &lt;input value="{{ today &#124; date:'yyyy-MM-dd' }}"&gt; | ```<input value="2017-06-03">``` |
| &lt;input value="{{ today &#124; date:format }}"&gt; | ```<input value="06/03">``` |

Pipes can be chained together:

| in HTML template | It is rendered in browser as: | 
| -------- | -------- |
|&lt;input value="{{ today &#124; date &#124; uppercase }}"&gt; | ```<input value="JUN 3, 2017">``` |

**Note:** You can overuse pipes and slow down your application, so you may want
to consider other ways of transforming the data within the component.  

## Binding Attribute 1: Set Attribute to a Variable Value

Brackets indicate **input binding**, data going from the component to the HTML.

Syntax:
```html
<input [value]="state">
```
This is the same as: 

```html
<input value={{state}}>
```
When you have a bracketed attribute, the string that is assigned is an expression 
that will be evaluated.  

| If defined in component | This is rendered in the browser | 
| ----- | --------|
| ```state="VA"``` | ```<input value="VA">```

**Note:**  One gotcha with element attributes in HTML is that sometimes you need to append ```attr.``` in front of the attribute when binding.

Short Answer:  Try the attribute with and without the ```attr.``` prefix until you 
get the result you want. 

The key is knowing what is an [HTML attribute versus a DOM property](https://angular.io/guide/template-syntax#html-attribute-vs-dom-property).
There are some notable exceptions (e.g., to bind _initially_ to the `disabled` property, you use `[attr.disabled]="boolexpr ? '' : null"` in order to use "attribute binding" in the template).


## Binding Attribute 2: Insert Attribute based on boolean value

Example, make the disabled attribute appear based on some value being true.  For example, we would want the userId field disabled for entry if a user ID exists.  

Syntax: 
```html
<input formControlName="userId" [attr.disabled]="userId">
```
| If defined in component | This is rendered in the browser | 
| ----- | --------|
|```userId="valid user"``` | ```<input formControlName="userId" disabled>``` |
|```userId=null``` | ```<input formControlName="userId">``` |



## Binding Attribute 3: Insert a Class based on boolean value

Example, add or delete the **hidden** class based on the truthiness of a variable

Syntax:
```html
<input class="other" [class.hidden]="hideUserId" formControlName="userId">
```
When you have a bracketed attribute, the string that is assigned is an expression 
that will be evaluated.


| If defined in component | This is rendered in the browser | 
| ----- | --------|
| ```hideUserId==true``` | ```<input class="other hidden" formControlName="userId">```|
| ```hideUserId==false``` | ```<input class="other" formControlName="userId">```|

## Binding Attribute 4: Set a Style based on a Variable

Example, we'll maintain a variable that describes the number of pixels at the 
top margin and set that to the div's style.

Syntax: 
```html
<div [style.margin-top.px]="marginTop">
```

| If defined in component | This is rendered in the browser | 
| ----- | --------|
| ```marginTop=80``` | ```<div style="margin-top: 80px">```|

## Binding an Event: Trigger a function on a javascript event 

Parenthesis indicate **output binding**, something being sent from the DOM to the component.

There are multiple events in HTML that functions can be bound to.  In Javascript, 
they are prefixed with ```on```.  Common events that are used are, onclick(),
onmouseover(), onkeydown() and onchange().  A link to the full list can be found
at the end of this article.

Example, when a user clicks on a button, write a notification to the console.

Syntax:
```html
<button (click)="onClick()">Click Me</button>
```
Javascript events are prefixed with ``on`` and that is omitted when using 
the above syntax.

A function would have to be defined on the component to do something: 
```typescript
    onClick() {
        console.log( "Clicked!" );
    }
```
## Template Reference Variable

Sometimes it is useful to access a DOM element from another part of the template.
Suppose we have an input field for capturing the last name.  We could create a 
reference to the field like this: 
```html
<input #lastName placeholder="Last Name">
<button (click)="onClick(lastName.value)">Click Me</button>
```
Note that on the input tag we created a reference with the hash mark: ```#lastName```.

Then the call to **onClick()** is passed the contents of lastName you just created a
reference for with the #.

If you define onClick() in your component:
```typescript
  onClick( value ) {
    console.log( "Value is " + value );
  }
```

You can enter data into the output field, click on the button and see output in the
console.

## Handling Potential Nulls - Safe Navigation Operator (?.)

Let's say that you have in your template: 
```html
<p>The user's name is {{user.name}}<p>
```
If the variable **user** is at risk of being null, accessing **name** would cause
an error.   If you use the ```?.``` operator, it will simply render a blank when 
it detects a null:

```html
<p>The user's name is {{user?.name}}<p>
```
You can test this by looking at the console output with both variations.


**Next Tutorial:** [Pre-Built Structural Directives](./angular-directives.md)

## References
 * [Angular Reference to Template Syntax](https://angular.io/docs/ts/latest/guide/template-syntax.html)
 * [Angular Reference to Pipes](https://angular.io/docs/ts/latest/guide/pipes.html)
 * [List of all HTML DOM Events](https://www.w3schools.com/jsref/dom_obj_event.asp)
