# Angular Components

[Angular Tutorials](./README.md)<br>
[Angular Modules](./angular-modules.md)

*Est. Time: 19 Minutes*

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **tags-module**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout tags-module
yarn
```

## Understanding the Main App Component

A component represents a part of a screen. You can start out by defining
what the component contains with HTML code and styles. You can associate code
with the component so that it can display data dynamically.  Components usually
have a ```selector``` defined.  This is the name of the tag (e.g., `<app-root></app-root>`),
so when you include the tag in your HTML code, the component's HTML is placed wherever you
place the tag.

The main application component, **app.component.ts**, does exactly this:

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
```

Note that the **selector** is named **app-root** and that the variable
**title** is assigned the text 'app works!'.

In the templateUrl: **app.component.html**, You'll see that the **title** is
interpolated into the HTML.

```html
<!--The content below is only a placeholder and can be replaced.-->
<div style="text-align:center">
  <h1>
    Welcome to {{title}}!
  </h1>
  <img width="300" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==">
</div>
<h2>Here are some links to help you start: </h2>
<ul>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://angular.io/tutorial">Tour of Heroes</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://github.com/angular/angular-cli/wiki">CLI Documentation</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://blog.angular.io/">Angular blog</a></h2>
  </li>
</ul>

<router-outlet></router-outlet>

```

In the **index.html**, you'll see the tag **`<app-root>`**:
```html
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>MyApp</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <app-root>Loading...</app-root>
</body>
</html>
```

This is how you end up seeing **app works!** when you run ```ng serve```.

When you get comfortable with creating components, you'll be able to mock up
screens quickly. After adding routing, you can setup a clickable demo which
is useful for test driving proposed user interface before coding up the entire
application.

---
## Disabling the app.component.spec.ts

Testing high level components starts to be problematic when you add more components.
A different strategy for testing will be presented later.  For now, disable the test
by placing an **x** in front of ```describe(``` like this:
```typescript

import { AppComponent } from './app.component';

xdescribe('AppComponent', () => {      // <- place x in front of describe
  beforeEach(async(() => {
    TestBed.configureTestingModule({

      ...
      ...
      ...
```

## Creating a Header Component

Let's start by creating a header
component, so that we have a standard header for the application.
We'll create this under the **tags/** directory and make it part of the
**tags** module.

```shell
# Go into the src/apps/tags directory so that the new component is
# created under the tags module directory.
cd src/apps/tags
pwd

# The component will be a header that is shared.
ng g component header
```
The output will look like this:
```shell
installing component
  create src/app/tags/header/header.component.css
  create src/app/tags/header/header.component.html
  create src/app/tags/header/header.component.spec.ts
  create src/app/tags/header/header.component.ts
  update src/app/tags/tags.module.ts
```
Let's take a look at the generated files:

---
## header.component.ts

This is the main file for the Header component.  All the other generated files
are referenced from this component class.

```typescript
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
```
This is a shell component class.  Key things to look at are:

|      | description |
| ---- | ----------- |
| `selector` | This is the tag name of the component.  This component can be included in another HTML by inserting the tags like this: `<app-header></app-header>` |
| `templateUrl` | This is the filename of the HTML content.   You could alternatively put the HTML content directly into this file by assigning the value `template` instead of `templateUrl`. |
| `styleUrls` | This is an array of stylesheet filenames.  You could alternatively have your styles in a string array named `styles`.
| `constructor` | Typically you instantiate dependency injected items here.  This is discussed later. |
| `ngOnInit()` | Is the function that is called on initialization. Put your startup code here. |

---
## header.component.html

This contains the HTML for the header.   You can put any HTML code here you want.
You'll also be able to add dynamic content to this HTML.

```html
<p>
  header works!
</p>
```
Right now, this just contains placeholder content.  You'll want to put your
real header HTML here.

---
## tags.module.ts

If you look at **tags.module.ts** it has been updated with a reference to the
HeaderComponent. To make the HeaderComponent available when this module is
imported, we need to export the HeaderComponent. **Add the export line** that
is shown below.

You'll also need to add the MaterialModule in each module that uses the Angular Material
tags.

```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from '../material/material.module';     // add this line

@NgModule({
  imports: [
    CommonModule, MaterialModule      // add Material Module
  ],
  exports: [HeaderComponent],         // add this line
  declarations: [HeaderComponent]
})
export class TagsModule { }
```
**Note:** Whenever you update a module, you may need to restart ```ng serve``` so
that it can pickup the changes.

---
## Show the Header

Go to **src/app/app.component.html** and add the **<app-header>** tag.  We are
going to replace the original content with the following so you can see clearly
what is going on:
```html
<app-header></app-header>
<h1>
  {{title}}
</h1>
<router-outlet></router-outlet>
```

When you run `ng serve`, you'll see the text **'header works!'**

Now you can update the contents of **header.component.ts** to make a better
looking header.  Play with it now.

---
## Sprucing up the Header

Change the content of **header.component.html** to this toolbar example:
```html
<mat-toolbar color="primary">
  <span>MyApplication</span>

  <span class="fill-space"></span>

  <button mat-icon-button [matMenuTriggerFor]="mainmenu">
    <mat-icon>menu</mat-icon>
  </button>

</mat-toolbar>

<mat-menu #mainmenu="matMenu">
  <button mat-menu-item>Admin</button>
  <button mat-menu-item>Settings</button>
</mat-menu>
```

You should see your updated header now.  If you kept ```ng serve``` on, the
changes will appear automatically while you change the code.

Take this opportunity to make changes to the header and making your own footer
as well.

---
## Setup a "404 Page Not Found" Component

Let's create a default 404 page.  We'll make a very rudimentary one but it
should be customized for the organization.  it is an opportunity to create
a page that is friendly to the user.

We'll put this on the TagsModule.

```shell
cd src/app/tags
ng g component not-found
```

Now you'll want to make some modifications:

 1. For clarity, change the ```selector``` in **not-found.component.ts** from
    **app-not-found** to **page-not-found**.

 1. Replace contents of **not-found.component.html** with ```<h1>Oops! Page not found.</h1>```.

 1. Go to **tags.module.ts** and add **NotFoundComponent** to the **exports**
    array.

 1. **Restart** ```ng serve``` - this will pick up modifications to the modules.

 1. Test access by inserting the tag ```<page-not-found></page-not-found>``` into
    **app.component.html**.  If it doesn't appear, you should open the console.
    Most of the time you missed the step to update the **tags.module.ts**.

 1. Once you've confirmed that the page-not-found tag works, take it out.  We
    will implement it properly in the routing tutorial.

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **base-components**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout base-components
```

**Next Tutorial:** [Basic Angular Services](./angular-service-basic.md)
