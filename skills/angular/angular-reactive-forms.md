# Reactive Forms

[Angular Tutorials](./README.md)<br>
[Built-in Structural Directives](./angular-directivess.md)


## Prerequisite

You should have some basic Jasmine testing experience.  Spending an hour or so on
[exercism.io](http://exercism.io/languages/javascript/about) doing exercises
for Javascript will get you familiar quickly.

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **user-service**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout user-service
yarn
```

## Overview

There are two ways to build forms, **Template forms** and **Reactive forms**.

Template forms take less code but have asynchronous behavior, and you must be
very familiar with how Angular works to test with it.   Reactive forms has
synchronous behavior and doesn't have the timing issues related to
template based forms.

In this sample, we'll create a demo entry page using template forms that
allows you to add, update and remove users from a system.

## How it works

You will be create a set of control objects in your component class that match
the form controls in your HTML.  These control objects are always in sync with
the form controls, so you'll be able to query and set values on the control
objects and they will be directly reflected on the HTML page.

## Create a User Module

The user module will encapsulate all the components related to managing users.
Create the user module under src/app:
```shell
cd src/app
pwd                                 // make sure you are in src/app

ng g module user

# Output should look like this
installing module
  create src/app/user/user.module.ts
```

go to **app.module.ts** and add UserModule to the imports:
```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TagsModule } from 'app/tags/tags.module';
import { UserServiceModule } from 'app/user-service/user-service.module';
import { UserModule } from './user/user.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TagsModule,
    UserModule,                     // <-- Add this line
    UserServiceModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Create the Components


We'll first create a component class for **user** and **userlist** under the src/apps/user so
they are part of the UserModule:

```shell
cd src/apps/user
ng g component user
ng g component userlist

# You will see output like this
installing component
  create src/app/user/user/user.component.css
  create src/app/user/user/user.component.html
  create src/app/user/user/user.component.spec.ts
  create src/app/user/user/user.component.ts
  update src/app/user/user.module.ts

installing component
  create src/app/user/userlist/userlist.component.css
  create src/app/user/userlist/userlist.component.html
  create src/app/user/userlist/userlist.component.spec.ts
  create src/app/user/userlist/userlist.component.ts
  update src/app/user/user.module.ts

```

This command automatically updated the declarations in **user.module.ts**.  However, it did not
export these files to make it available externally. Export them now as shown below:

```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserComponent } from './user/user.component';
import { UserlistComponent } from './userlist/userlist.component';


@NgModule({
  imports: [
     CommonModule
  ],
  exports: [UserComponent, UserlistComponent],
  declarations: [UserComponent, UserlistComponent]
})

```
To immediately display this page:

1. In the user.component.ts, you'll see that the 'selector' is set to
    'app-user' and 'app-userlist' by default.  To make this component show up, add these tag to
    app.component.html page.
2. start ```ng serve``` to see your page.

This is a quick way to confirm what you are creating.  We will eventually do this nicely with
Angular Routing.


## Import ReactiveFormsModule

Add import ReactiveFormsModule to **user.module.ts** so that the library is
available to the components in the UserModule.  The most visible thing this module gives you:

 1. Makes the FormBuilder a provider that is available to all modules via
    dependency injection.
 1. Gives you directives that allow you to assign formGroup on the <form> tag
    and and formControl on any of the control tags in your HTML.

```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserlistComponent } from './userlist/userlist.component';

import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
     CommonModule, MaterialModule, ReactiveFormsModule                      // <-- Add MaterialModule and ReactiveFormsModule
  ],
  exports: [UserComponent, UserlistComponent],
  declarations: [UserComponent, UserlistComponent]
})
export class UserModule { }


```

## Create the Form Objects in your Component

We recommend that you create the Component side objects before you create the
HTML form that will be bound to it.  That way you can incrementally build the
project up without errors.

| Type | Description |
| ---- | ----------- |
| [FormGroup](https://angular.io/docs/ts/latest/api/forms/index/FormGroup-class.html) | contains FormControls and the top level FormGroup is bound to the HTML form. |
| [FormControl](https://angular.io/docs/ts/latest/api/forms/index/FormControl-class.html) | is created for each control that we are managing on the form. |
| [FormBuilder](https://angular.io/docs/ts/latest/api/forms/index/FormBuilder-class.html) | reduces the typing needed to build up form groups with controls. |
| [Validators](https://angular.io/docs/ts/latest/cookbook/form-validation.html) | provide ways to add validation to fields. |

In your UserComponent class file, add these imports:

```typescript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';   // <-- Add this import

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  title  = 'Add User';
  userForm: FormGroup;                   // <- Create this userForm Form Group,
                                                 //    this will be bound to the <form> tag in your HTML

  constructor(private fb: FormBuilder) {         // <- Get the FormBuilder provider via Dependency Injector
  }

  ngOnInit() {
    this.userForm = this.fb.group({              // <- Use the FormBuilder to build the userForm up.
                                                 //    Each entry represents a FormControl that will
                                                 //    be created in the FormGroup

      firstName: '',                             // <- Assign a default string only
      lastName: [ '', Validators.required ]      // <- Assign a default string and validator to last name

    });
  }

}
```


This is the same code without the comments:

```typescript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  title  = 'Add User';
  userForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      firstName: '',
      lastName: [ '', Validators.required ]
    });
  }

}



```


## Create the HTML Form

Now we'll create the HTML form and bind it's parts to the userForm FormGroup we created in the Component.  Let's first add some styles
to the **user.component.scss**:

```css
.example-form {
    min-width: 150px;
    max-width: 500px;
    width: 100%;
}

.standard-padding {
  padding: 20px;
}

.full-width {
  width: 100%;
}

.example-tab-group {
  border: 1px solid #e8e8e8;
}

```

Then add a basic form to **user.component.html**:

```html
<div align="center" class="standard-padding">

    <form class="example-form" [formGroup]="userForm" novalidate> <!-- We bind the userForm FormGroup object to the formGroup
                                                                       attribute on the form.  The novalidate is an HTML attribute
                                                                      suppresses the HTML validation                              -->

    <mat-card class="full-width">
      <mat-card-title-group>
        <mat-card-title>
          {{title}}
        </mat-card-title>
      </mat-card-title-group>

      <mat-tab-group>

        <mat-tab label="Base Information">
          <div class="standard-padding">

            <mat-form-field class="full-width">
             <!-- This input is bound to firstName by formControlName.  -->
              <input matInput placeholder="First Name:" class="form-control" formControlName="firstName">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="Last Name:" class="form-control" formControlName="lastName">
            </mat-form-field>
          </div>
        </mat-tab>


      </mat-tab-group>

    </mat-card>
    <!-- This section shows how to query information from the FormGroup -->
    <p>Form value: {{ userForm.value | json }}</p>
    <p>Form status: {{ userForm.status | json }}</p>

  </form>
</div>

```


**Note** the json filter (`| json`) on the Form value and status at the bottom of the HTML.   This takes the object value
and converts it into JSON so that it is output in a readable form.

Here is the code without the comments:

```html
<div align="center" class="standard-padding">

  <form class="example-form" [formGroup]="userForm" novalidate>

    <mat-card class="full-width">
      <mat-card-title-group>
        <mat-card-title>
          {{title}}
        </mat-card-title>
      </mat-card-title-group>

      <mat-tab-group>

        <mat-tab label="Base Information">
          <div class="standard-padding">

            <mat-form-field class="full-width">
              <input matInput placeholder="First Name:" class="form-control" formControlName="firstName">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="Last Name:" class="form-control" formControlName="lastName">
            </mat-form-field>
          </div>
        </mat-tab>


      </mat-tab-group>

    </mat-card>

    <p>Form value: {{ userForm.value | json }}</p>
    <p>Form status: {{ userForm.status | json }}</p>

  </form>
</div>

```

You can see the page by running ```ng serve```.  When you type into the field, you can observe the status
of the userForm and the value it has in it.

## Creating Sub FormGroups

For address, we'll create a sub-form so that we organize the information better.

Update the UserComponent class as follows:

```typescript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { states, roles, titles, User } from 'app/user-service/user';   // <- Import static lists and User class
                                                                       //    This will be used for lists on the form

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  states = states;                                            // <- assign states list to this.states
  title  = 'Add User';
  userForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      firstName: '',
      lastName: [ '', Validators.required ],

       address: this.fb.group({       // <-- add this nested FormGroup for address
        street: '',
        city: '',
        state: '',
        zip: ''
      }),
    });
  }

}

```

Update the HTML to the following:

```html
<div align="center" class="standard-padding">

  <form class="example-form" [formGroup]="userForm" novalidate>

    <mat-card class="full-width">
      <mat-card-title-group>
        <mat-card-title>
          {{title}}
        </mat-card-title>
      </mat-card-title-group>


      <mat-tab-group>
        <mat-tab label="Base Information">
          <div class="standard-padding">

            <mat-form-field class="full-width">
              <input matInput placeholder="First Name:" class="form-control" formControlName="firstName">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="Last Name:" class="form-control" formControlName="lastName">
            </mat-form-field>
          </div>

        </mat-tab>

        <mat-tab label="Address" formGroupName="address">
          <div class="standard-padding">
            <mat-form-field class="full-width">
              <input matInput placeholder="Street:" class="form-control" formControlName="street">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="City:" class="form-control" formControlName="city">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="State:" class="form-control" formControlName="state">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="Zip Code:" class="form-control" formControlName="zip">
            </mat-form-field>
          </div>
        </mat-tab>

      </mat-tab-group>
    </mat-card>

    <p>Form value: {{ userForm.value | json }}</p>
    <p>Form status: {{ userForm.status | json }}</p>

  </form>
</div>
```

You can see all the fields you can access on the FormControl object here: https://angular.io/docs/ts/latest/api/forms/index/AbstractControl-class.html.

Read over these fields and their brief descriptions starting here: https://angular.io/api/forms/AbstractControl#members.
This will give you a good grounding for understanding what properties you can inspect in response to events.

### *ngFor

To set this up, let's update the **state** array in **user.ts** to have both the
state code and the state description in each entry:

```typescript
  export const states = [
    { code: 'CA', desc: 'California' },
    { code: 'DC', desc: 'District of Columbia' },
    { code: 'FL', desc: 'Florida' },
    { code: 'MD', desc: 'Maryland' },
    { code: 'NY', desc: 'New York' },
    { code: 'VA', desc: 'Virginia' }
];

```

*ngFor is a directive that will repeat the tag it is placed on.  Don't forget the asterisk!

```typescript
            <mat-form-field class="full-width">
                <mat-select placeholder="State" formControlName="state">
                  <mat-option *ngFor="let state of states" [value]="state.code">
                    {{state.code}} {{ state.desc }}
                  </mat-option>
                </mat-select>
            </mat-form-field>
```

The argument ```"let state of states"``` cycles through the **states** list you setup in the UserComponent.
The value **state** is updated on each iteration with a differnt state on the **states** list, allowing you to
use that object to populate sections of the HTML.

### Property Bind - [property]

`[value]="state.code"`

**value** is an attribute on the `<mat-options>` tag and indicate the value that will be
placed into the form control field when the option is selected.   Brackets allow input into a property.   The expression in
quotes is evaluated and placed into the value attribute.   If `state={code:"VA", desc:"Virginia"}`, the tag that is rendered would be:

```html
              <mat-option value="VA">
                VA Virginia
              </mat-option>
```

## Completing the User Entry Page

Based on the information provided:

1. Build out the entry form to include all the data except for the list of phones.
2. Use roles and titles list on the page just like we did for states.

Do it before copying the solution provided below.

## user.component.ts - with all fields
```typescript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { states, roles, titles, User } from '../../user-service/user';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  states = states;
  roles  = roles;
  titles = titles;
  title = 'Add User';

  userForm: FormGroup;

  constructor(private fb: FormBuilder) {}


  ngOnInit() {
    this.userForm = this.fb.group({
      userId: [ '', Validators.required ],
      firstName: '',
      lastName: [ '', Validators.required ],
      role: '',
      title: '',
      active: true,

      address: this.fb.group({
        street: '',
        city: '',
        state: '',
        zip: ''
      }),

    });

  }


}

```

## user.component.html - with all fields
```html

<div align="center" class="standard-padding">

  <form class="example-form" [formGroup]="userForm" novalidate>

    <mat-card class="full-width">
      <mat-card-title-group>
        <mat-card-title>
          {{title}}
        </mat-card-title>
      </mat-card-title-group>

      <mat-tab-group>
        <mat-tab label="Base Information">
          <div class="standard-padding">


            <mat-form-field class="full-width">
              <input matInput placeholder="User ID:" class="form-control" formControlName="userId">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="First Name:" class="form-control" formControlName="firstName">
            </mat-form-field>

            <mat-form-field class="full-width">
              <input matInput placeholder="Last Name:" class="form-control" formControlName="lastName">
            </mat-form-field>

            <mat-form-field class="full-width">
              <mat-select placeholder="Roles" formControlName="role">
                <mat-option *ngFor="let role of roles" [value]="role">
                  {{role}}
                </mat-option>
              </mat-select>
            </mat-form-field>

            <mat-form-field class="full-width">
              <mat-select placeholder="Title" formControlName="title">
                <mat-option *ngFor="let title of titles" [value]="title">
                  {{title}}
                </mat-option>
              </mat-select>
            </mat-form-field>

            <div class="full-width" style="text-align:left">
              <mat-checkbox formControlName="active">Active</mat-checkbox>
            </div>

          </div>

        </mat-tab>

        <mat-tab label="Address" formGroupName="address">
          <div class="standard-padding">
              <mat-form-field class="full-width">
                  <input matInput placeholder="Street:" class="form-control" formControlName="street">
                </mat-form-field>

                <mat-form-field class="full-width">
                  <input matInput placeholder="City:" class="form-control" formControlName="city">
                </mat-form-field>

                <mat-form-field class="full-width">
                    <mat-select placeholder="State" formControlName="state">
                      <mat-option *ngFor="let state of states" [value]="state.code">
                        {{state.code}} {{ state.desc }}
                      </mat-option>
                    </mat-select>
                </mat-form-field>

                <mat-form-field class="full-width">
                  <input matInput placeholder="Zip Code:" class="form-control" formControlName="zip">
                </mat-form-field>
          </div>
        </mat-tab>

      </mat-tab-group>

    </mat-card>

    <p>Form value: {{ userForm.value | json }}</p>
    <p>Form status: {{ userForm.status | json }}</p>


  </form>
</div>

```

## Disable the generated test specs

In the `user.component.spec.ts` and `userlist.component.spec.ts`, change the top-most `describe` to `xdescribe` to
mark the entire suite pending.

Test it in the bash terminal before committing your changes:

```shell
npm test
```

## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **reactive-forms**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout reactive-forms
```

**Next Tutorial:** [Routing - Part 1](./angular-routing-1.md)

### References

 * https://angular.io/docs/ts/latest/guide/reactive-forms.html
 * https://angular.io/docs/ts/latest/cookbook/form-validation.
 * https://angular.io/docs/ts/latest/guide/template-syntax.html
 * https://material.angular.io/

