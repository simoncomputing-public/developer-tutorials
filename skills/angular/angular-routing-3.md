# Angular Routing - Part 3

[Angular Tutorials](./README.md)<br>
[Routing - Part 2](./angular-routing-2.md)

## Sample Starter Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **routing-2**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout routing-2
yarn
```

## Creating an Angular Materials Table

1) We'll start by updating the **userlist.component.ts** with a UserDataSource.  This is
a simple class that returns an Observable object.  Add the the import to and the
following snippet at the bottom of the source file.

```typescript
// Paste this in the import section
import { Observable } from 'rxjs/Observable';

...
...
...
// Paste this  at the bottom of our source code
export class UserDataSource extends DataSource<any> {
  constructor( private data: User[] ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<User[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}
```

2) Now we'll add an array of column names to the **UserListComponent** class:

```typescript
  displayedColumns = [ 'userId' ];
```
This will defines which of the columns will be visible and what order they will appear.
For now we will just display one column and add the rest later.

3) The last change is to add a data source variable to the **userListComponent** and
instantiate the data source:

```typescript
import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/user-service/user.service';
import { User } from 'app/user-service/user';
import { DataSource } from '@angular/cdk/collections';    // <-- Add this import
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
  users: User[];
  displayedColumns = [ 'userId' ];

  dataSource: UserDataSource;                             //<=== Add this variable
  constructor( private service: UserService ) { }

  ngOnInit() {
    this.users = this.service.getUsers();
    this.dataSource = new UserDataSource( this.users );   // <=== Initialize the data source here
  }

}

export class UserDataSource extends DataSource<any> {
  constructor( private data: User[] ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<User[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}
```

## Updating the HTML

Add the Material table entry as shown below in **userlist.component.html**:

```html
<mat-card>
  <h1>User List</h1>

  <!-- Create a container that is raised from surface -->
  <div class="example-container mat-elevation-z8">

    <!-- Note that the dataSource is specified here -->
    <mat-table #table [dataSource]="dataSource">

      <!-- We've defined a single column, User ID-->
      <ng-container matColumnDef="userId">
        <mat-header-cell *matHeaderCellDef> User ID </mat-header-cell>
        <mat-cell *matCellDef="let user"> {{user.userId}} </mat-cell>
      </ng-container>

      <!-- The following lines defines the column headers -->
      <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>

      <!-- The following line specifies the cell data -->
      <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>

    </mat-table>

  </div>

  <br/>
  <br/>
  <button mat-raised-button color="primary" routerLink="/add-user">Add User</button>

  <p>Users: {{service.getUsers() | json }}</p>

</mat-card>

```

## Adding the rest of the columns

Update the **userlist.component.html** with the rest of the base columns:

```typescript
<mat-card>
  <h1>User List</h1>


  <div class="example-container mat-elevation-z8">

    <mat-table #table [dataSource]="dataSource">

      <ng-container matColumnDef="userId">
        <mat-header-cell *matHeaderCellDef> User ID </mat-header-cell>
        <mat-cell *matCellDef="let user"> {{user.userId}} </mat-cell>
      </ng-container>

      <ng-container matColumnDef="lastName">
          <mat-header-cell *matHeaderCellDef> Last Name </mat-header-cell>
          <mat-cell *matCellDef="let user"> {{user.lastName}} </mat-cell>
      </ng-container>

      <ng-container matColumnDef="firstName">
          <mat-header-cell *matHeaderCellDef> First Name </mat-header-cell>
          <mat-cell *matCellDef="let user"> {{user.firstName}} </mat-cell>
      </ng-container>

      <ng-container matColumnDef="role">
          <mat-header-cell *matHeaderCellDef> Role </mat-header-cell>
          <mat-cell *matCellDef="let user"> {{user.role}} </mat-cell>
      </ng-container>

      <ng-container matColumnDef="title">
          <mat-header-cell *matHeaderCellDef> Title </mat-header-cell>
          <mat-cell *matCellDef="let user"> {{user.title}} </mat-cell>
      </ng-container>


      <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
      <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>

    </mat-table>

  </div>

  <br/>
  <br/>
  <button mat-raised-button color="primary" routerLink="/add-user">Add User</button>

  <p>Users: {{service.getUsers() | json }}</p>

</mat-card>

```

To make all the columns show, go to **userlist.component.ts** and update the **displayedColumns** array:

```typescript
  displayedColumns = [ 'userId', 'firstName', 'lastName', 'role', 'title' ];
```


## Allowing Update of Users

We can reuse the UserComponent to edit a user but some changes need to be made:

1. We need the ability to specify the user ID that needs to be updated to the UserComponent.
1. The title would need to change to "User Update" instead of "Add User" on the UserComponent page if we are in edit mode.
1. We should call separate functions for Add and Update.
1. The **userId** is a key field, so we should disable changing it on Update.


#### Creating a Route with Parameter

Let's start out by establish a route that allows a user ID to be passed on the URL.  We should
create a specific path for updating users.   The call would look like this: http://localhost:4200/user/<userId>

Go to the **app-routing.module.ts** and add a new route to the table:
```typescript
const routes: Routes = [
  { path: 'update-user/:userId', component: UserComponent },   // <-- Add this entry
  { path: 'add-user', component: UserComponent },
  { path: 'userlist', component: UserlistComponent },
  { path: '', redirectTo: 'userlist', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];
```
The colon precedes a variable indicating a parameter that will be passed to the UserComponent.

#### Getting the Parameter

You'll need to access the ActivatedRoute, which you'll inject using a constructor.   It has an attribute named ```param``` which
is an ```Observable```.   You can subscribe to the Observable in order to respond when it returns a value.

We'll also set up our component to `unsubscribe` when the component is destroyed.

Start by updating the **user.component.ts** file:

```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';  // <-- add `OnDestroy`
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';      // <- Import ActivatedRoute from @angular/router
import { ISubscription } from 'rxjs/Subscription';             // <-- add `ISubscription`

import { Observable } from 'rxjs/Rx';

import { states, roles, titles, User } from '../../user-service/user';
import { UserService } from '../../user-service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {      // <-- add `OnDestroy`
  error = null;
  states = states;
  roles = roles;
  titles = titles;
  title = 'Add User';

  private userId: string;                                     // <- Initialize userId variable in the Component class
  private paramSubscription: ISubscription;                    // <- Store subscription so we can cancel it later

  userForm: FormGroup;

  constructor(private fb: FormBuilder,
    private service: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {      // <- Inject ActivatedRoute
  }

  delayedClear() {
    const timer = Observable.timer(5000);
    timer.subscribe(x => this.error = null);
  }


  addUser() {
    this.error = this.service.addUser(this.userForm.value);

    if (this.error) {
      this.delayedClear();
      return;
    }

    this.router.navigate(['/userlist']);
  }


  ngOnInit() {
    this.userForm = this.fb.group({
      userId: ['', Validators.required],
      firstName: '',
      lastName: ['', Validators.required],
      role: '',
      title: '',
      active: true,

      address: this.fb.group({
        street: '',
        city: '',
        state: '',
        zip: ''
      }),

    });

    this.paramSubscription = this.activatedRoute.params.subscribe(this.processParams); // <- Add this line to ngOnInit().
  }

  /**
   * Process the array of parameters received on entry into this component.
   * The fat arrow notation is necessary to make sure this. points to the class
   * and not the function that will actually call it.
   *
   * @param params
   */
  processParams = ( params ) => {
    this.userId = params['userId'];

    console.log( "UserID: " + this.userId );
    // if userId is empty, we are in add mode, exit.
    if ( !this.userId ) return;

    // if userId exists, change the title to indicate we are in update mode
    this.title = `User Update`;
    let user = this.service.getUser( this.userId );

    // if the userId passed in is not found, display na error.
    if ( !user ) {
      this.error = `User ID="${this.userId}" could not be found.`;
      this.delayedClear();
      return;
    }

    // Initialize the form with the current user's setting
    this.userForm.setValue( user );
  }

  /**
   * unsubscribes from the subscription established in ngOnInit()
   */
  ngOnDestroy() {
    if (this.paramSubscription) this.paramSubscription.unsubscribe();
  }


}


```

 When you subscribe to an observable, it is good practice to unsubscribe when your component is done with it.
 There are a few exceptions (ActivatedRoute.params is one of them), but it doesn't hurt, and is a good habit to be in.

Finally, we need to add a link to **userlist.component.html** to call /update-user/<userId> from each user line:
```html
      <ng-container matColumnDef="actions">
          <mat-header-cell *matHeaderCellDef> Actions </mat-header-cell>
          <mat-cell *matCellDef="let user">
              <button mat-icon-button routerLink="/update-user/{{user.userId}}">
                <mat-icon title="Edit user" aria-label="Edit user">edit</mat-icon>
              </button>
          </mat-cell>
      </ng-container>
```

In the **userlist.components.ts**, add the **actions** column to the **displayedColumns** field.

At this point, you can select a user off of the user list and navigate to UserComponent with a userId.   In the
```processParams()``` function, if a **userId** is found, then we will treat this as edit mode and retrieve the
user's information.

Now we need to modify the current add function to save for both add and update mode, based on the presence of the userId.
In **user.component.ts**, we'll rename ```addUser()``` to  ```saveUser()``` function and call updateUser() if userId exists:
```typescript
  saveUser() {

    this.error = this.userId ? this.service.updateUser( this.userForm.value ) : this.service.addUser( this.userForm.value );

    if ( this.error ) {
      this.delayedClear();
      return;
    }

    this.router.navigate( ['/userlist'] );
  }
```

We'll want the save button to either say "Add" or "Update" based on whether the userId is present.  In the **user.component.html**
update the button to look like this:

```html
      <button mat-raised-button
              color="primary"
              [disabled]="userForm.status === 'INVALID'"
              (click)="saveUser()">
              <span *ngIf="!userId">Add</span>
              <span *ngIf="userId">Update</span>
      </button>
```
Note that we used ```*ngIf``` to conditionally show the appropriate text.


## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **routing-3**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout routing-3
```

## References

* https://angular.io/docs/ts/latest/guide/router.html
* https://angular.io/docs/ts/latest/api/router/index/ActivatedRoute-interface.html
* https://stackoverflow.com/questions/34513558/angular-2-0-and-modal-dialog
* [Angular: Don't forget to unsubscribe()](http://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/)
* http://reactivex.io/learnrx/

