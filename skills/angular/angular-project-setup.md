## Angular Project Setup

[Angular Tutorials](./README.md)

This tutorial guides you through the creation of a shell application with
the changes necessary to make the Continous Integration/Continuous Deployment
(CI/CD) with Jenkins work.

CI/CD allows you to create an automated build process that can be triggered by
a check-in, running the build process and automated tests.  If there are errors,
the team is emailed so that they can fix the problems immediately.

When you are done, you should have a running basic application that you can
build on.  It will be deployable to Jenkins.

### Steps

Skip any steps that do not apply to you.
1. [Install NodeJS](../setup/node.md)
2. [Install Yarn](../setup/yarn.md)
3. [Install Angular CLI](../setup/angular.md)

#### Create project

Navigate to the PARENT DIRECTORY where you want to create a new Angular project (as its own sub-folder).

```shell
# Create project with Routing module
ng new my-app --routing --style=scss

# navigate into the project directory
cd my-app

# Bring in SASS library
npm install node-sass

# start up the server
ng serve

# go to the browser at the reported port (e.g., http://localhost:4200) to make sure the scaffolded site loads
# then press CTRL-C to stop the server

# run the linter
ng lint --type-check

# run unit tests
ng test

# in the browser window that launches, ensure that the one test to check for "App Works!" passes
# press CTRL-C to stop the server

# run e2e tests
ng e2e

# end-to-end tests should pass as well.
```

#### Add versions to `Readme.md`

Because you are using `yarn` as the dependency manager, there will be a `yarn.lock` file in the root of the project
that list allnode modules in the project as well as the versions and the dependencies that are installed.

Although the `yarn.lock` file is used to help teams stay in synch with the installed packages, it is also a handy
documentation tool. But it does not denote the NodeJS, Yarn, or Angular CLI versions in use. Let's add those to the `Readme.md`
file.

  1. Open the `Readme.md` file for editing
  1. List the versions in the opening section:
    * `@angular/cli` (`ng --version -g`)
    * `yarn` (`yarn --version -g`)

**Hint:** In Visual Studio Code, you can paste in the version information from Angular and put a bullet in front of each line by using the multi-line edit feature:

1. Put your cursor on the first line
1. While holding down the **Option** and **Command** buttons, press the down arrow for every line you want the bullet to appear.
1. Type an **astrisks + space**
1. Press **escape** to get out of the multi-line edit mode.

#### Commit the initial project

This is a convenient point to commit the initial project. After this point, we will be adding a few more packages
to make continuous integration (on a system without a browser) easier to work with, as well as installing Twitter Bootstrap.

```shell
// 1. see what you are about to commit
git status

// 2. if you see any files in the status list that should NOT be added (like IDE files), add them to the .gitignore file

// 3. Stage the files
git add .

// 4. Commit locally
git commit -m "Initial commit"

// 5. Optionally, push the project into a remote central repository (on your own)
```

#### Prepare project for CI

In this step, we want to make sure the unit tests can be executed in an environment that does not have a graphic browser.
In other words, we want to eliminate the dependency on the Chrome Browser for running unit tests.

When you finish this setup, you should immediately prepare to run your build on a Jenkins server inside a Docker container
in order to verify that the setup is correct, and to troubleshoot any issues. We'll walk through those steps.

The terminal steps below assume Mac OS X. Please adjust if you are using a different operating system.

 1. add the **karma-spec-reporter** and **karma-phantomjs-launcher** (reports to console instead of browser)

   ```shell
   npm install --save-dev karma karma-jasmine karma-spec-reporter karma-phantomjs-launcher
   ```

 1. (optional-if using Docker for CI) upgrade the **karma-chrome-launcher** (must be version 2.2.0 or higher)

    ```shell
    npm update --save-dev karma-chrome-launcher
    ```

 1. Edit the **src/polyfills.ts** file and uncomment the following lines

    ```typescript
    import 'core-js/es6/object';
    import 'core-js/es6/string';
    import 'core-js/es6/array';
    import 'core-js/es6/set';
    ```

 1. Edit the **karma.conf.js** file:
  * Change plugins: `karma-chrome-launcher` to `karma-phantomjs-launcher` (unless using Docker for CI)
  * Change plugins: `karma-jasmine-html-reporter` to `karma-spec-reporter`
  * Change reporters:
    * Change 'progress' to 'spec'
    * Remove `kjhtml` as well as the preceding comma
  * Change browsers: `Chrome` to `PhantomJS`
    * (or if using Docker for CI) Change the `browsers` to "ChromeHeadless"

    ```javascript
    // Karma configuration file, see link for more information
    // https://karma-runner.github.io/0.13/config/configuration-file.html

    module.exports = function (config) {
      config.set({
        basePath: '',
        frameworks: ['jasmine', '@angular/cli'],
        plugins: [
          require('karma-jasmine'),
          require('karma-phantomjs-launcher'),                       // <-- updated
          //require('karma-chrome-launcher'),                        // (use this instead if using Docker for CI)
          require('karma-spec-reporter'),                            // <-- Updated
          require('karma-coverage-istanbul-reporter'),
          require('@angular/cli/plugins/karma')
        ],
        client:{
          clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        files: [
          { pattern: './src/test.ts', watched: false }
        ],
        preprocessors: {
          './src/test.ts': ['@angular/cli']
        },
        mime: {
          'text/x-typescript': ['ts','tsx']
        },
        coverageIstanbulReporter: {
          reports: [ 'html', 'lcovonly' ],
          fixWebpackSourcePaths: true
        },
        angularCli: {
          environment: 'dev'
        },
         reporters: ['spec'],                                   // <-- Updated
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],                                // <-- Updated
        //browsers: ['ChromeHeadless'],                         // (use this instead if using Docker for CI)
        singleRun: false
      });
    };
    ```

 1. Edit the **protractor.conf.js** file:
  * Add `chromeOptions` to the `capabilities` section

    ```javascript
    // Protractor configuration file, see link for more information
    // https://github.com/angular/protractor/blob/master/lib/config.ts

    const { SpecReporter } = require('jasmine-spec-reporter');

    exports.config = {
      allScriptsTimeout: 11000,
      specs: [
        './e2e/**/*.e2e-spec.ts'
      ],
      capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {                              // <-- add this section
          'args': [ '--headless', '--disable-gpu']      // <--
        }                                               // <--
      },
      directConnect: true,
      baseUrl: 'http://localhost:4200/',
      framework: 'jasmine',
      jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        print: function() {}
      },
      beforeLaunch: function() {
        require('ts-node').register({
          project: 'e2e/tsconfig.e2e.json'
        });
      },
      onPrepare() {
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
      }
    };

    ```

 1. Edit the **package.json** file, "scripts", "test" and "test-live-reload":
  * change "test" to "test-live-reload"
  * add "test" with value "ng test --single-run --reporters spec"

    ```json
    "scripts": {

      ...

      "test": "ng test --single-run --reporters spec",
      "test-live-reload": "ng test",

      ...

    }
    ```
  >Note: Scripts are run by calling npm with script name like this: ```npm test```, or `npm run test-live-reload`*

 1. Verify that `npm test` runs once and provides successful test output like this:

    ```shell
      AppComponent
        ✓ should create the app
        ✓ should have as title 'app works!'
        ✓ should render title in a h1 tag

    PhantomJS 2.1.1 (Mac OS X 0.0.0): Executed 3 of 3 SUCCESS (0.121 secs / 0.198 secs)
    TOTAL: 3 SUCCESS

    ```

 1. Verify that `ng e2e` runs and provides successful test output like this:

    ```shell
    ** NG Live Development Server is running on http://localhost:49152 **
    (node:7689) DeprecationWarning: os.tmpDir() is deprecated. Use os.tmpdir() instead.
    Hash: 8d8630bea40d47f27988
    Time: 10837ms
    chunk    {0} polyfills.bundle.js, polyfills.bundle.js.map (polyfills) 227 kB {4} [initial] [rendered]
    chunk    {1} main.bundle.js, main.bundle.js.map (main) 4.82 kB {3} [initial] [rendered]
    chunk    {2} styles.bundle.js, styles.bundle.js.map (styles) 9.89 kB {4} [initial] [rendered]
    chunk    {3} vendor.bundle.js, vendor.bundle.js.map (vendor) 2.76 MB [initial] [rendered]
    chunk    {4} inline.bundle.js, inline.bundle.js.map (inline) 0 bytes [entry] [rendered]
    webpack: Compiled successfully.
    [13:48:25] I/update - chromedriver: file exists /Users/brightgarden/git/waterford/ui/node_modules/webdriver-manager/selenium/chromedriver_2.31.zip
    [13:48:25] I/update - chromedriver: unzipping chromedriver_2.31.zip
    [13:48:25] I/update - chromedriver: setting permissions to 0755 for /Users/brightgarden/git/waterford/ui/node_modules/webdriver-manager/selenium/chromedriver_2.31
    [13:48:25] I/update - chromedriver: chromedriver_2.31 up to date
    [13:48:26] I/launcher - Running 1 instances of WebDriver
    [13:48:26] I/direct - Using ChromeDriver directly...
    Spec started

      ui App
        ✓ should display message saying app works

    Executed 1 of 1 spec SUCCESS in 0.681 sec.
    [13:48:28] I/launcher - 0 instance(s) of WebDriver still running
    [13:48:28] I/launcher - chrome #01 passed
    ```

 1. Finally, commit your changes (required to test in Jenkins).

## Testing in Jenkins

It is recommended that you push your code to a repository that is built in Jenkins in order to verify that the changes
are effective. See the DevOps training guide for installing and using Jenkins.  You may also use an installation
that already exists.

The edits made in these instructions are based upon @angular/cli v1.0.0, and may or may not be fully effective in the
version that you are using.


## Sample Code

You can get a copy of the work completed so far by cloning from https://gitlab.com/simoncomputing-public/my-app and checkout the tag: **base-setup**
```shell
git clone git@gitlab.com:simoncomputing-public/my-app.git
git fetch orign --tags
git checkout base-setup
```

**Next Tutorial:** [Installing Angular Material](./angular-material.md)

## References

1. https://github.com/angular/angular-cli/wiki
2. https://blog.angular.io/the-past-present-and-future-of-the-angular-cli-13cf55e455f8

