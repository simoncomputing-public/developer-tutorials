# Creating a REST Tutorial using SpringBoot (JAR)

## Objective

The objective of this tutorial is to get you up and running as quickly as possible building REST services
using SpringBoot.   It is based on a project started with the [Spring Initializr](https://start.spring.io). 

This tutorial will walk you through a manual project build using Eclipse.  This approach was chosen so 
you can understand exactly how the project is structured.  

## Establish the basic project structure 

In Eclipse: 

1. Right click in the Projec Explorer and select **New | Project.. | General | Project**
2. Then select **Next**
3. Give the project a name and click the finish button (In this example, we'll use **sample-rest**).  
   This will create a shell project.  Please use "kabab-case", which is lower case with hyphens seperating words.
4. Create the basic maven directory structure shown below:

```shell
sample-rest/
    |
    +--- src/
            |
            +--- main/
            |       |
            |       +--- java/
            |       |
            |       +--- resources/
            |
            +--- test/
                    |
                    +--- java/
            
    +--- pom.xml

```

## Create the pom.xml

Paste the following contents into the **pom.xml**:

```xml

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>sample-rest</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>sample-rest</name>
    <description>Demo project for Spring Boot</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.7.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Spring Fox -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.7.0</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.7.0</version>
        </dependency>
        
        <!-- MyBatis -->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.1</version>
        </dependency>

        <!-- H2 Database -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>runtime</scope>
        </dependency>
        
        <!-- Groovy for Logback Configuration -->
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
        </dependency>
                
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <formats>
                        <format>xml</format>
                        <format>html</format>
                    </formats>
                </configuration>
            </plugin>

        </plugins>
    </build>


</project>


```

Key aspects to understand in the **pom.xml**

1. ```<groupId>```  Specifies an ID ofor the group that this project falls under.  We recommend using the parent
   package name for the project.
1. ```<artifactId>```   The project name for the application or library being built.   For applications, 
    use kebab case (lowercase with hyphens separating words.)   For libraries, use the full package name the library
    falls under.  This way all code is traceable to their JAR file.
1. ```<packaging>``` This is set to **jar** for this example.  You would set this to **war** if you want to           create a deployable WAR file, but you'll have to make some [adjustments to the project](https://spring.io/guides/gs/convert-jar-to-war/) to make that work. 

Note that under ```<dependencies>```, we've included optional dependencies for: 

  1. **SpringFox/Swagger UI** which automatically documents REST interfaces and provides a nice user interface for testing.
  1. **MyBatis** for object relational mapping - database access.
  1. **Groovy** for the Groovy based LogBack configuration.   
   

Under plugins, we've also added **Cobertura** for code coverage analysis.   To execute, just run: 
**mvn cobertura:cobertura** 

This will generate HTML files with code coverage results.
   
**Convert this project into a Maven project** by right-clicking on the project name and selecting **Configure | Convert to Maven**
   
   
## Configuring your Application

The application configuration files should be stored in the root package of your project, in this example it is:
**com.example.samplerest**

While you can put all configuration information in a single file, it is helpful to separate configuration information by the area of concern.   In our example, we'll create a separate configuration file from the main 
Application class for Swagger.

Let's start by creating the main configuration class, which is traditionally named **Application.java** in the main package: 

```java
package com.example.samplerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```


Then we'll create the **SwaggerConfiguration.java** file to handle Swagger:

```java
package com.example.samplerest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Sample REST API")
            .description("SAMPLE REST API")
            .license("MIT license")
            .licenseUrl("https://opensource.org/licenses/MIT")
            .termsOfServiceUrl("")
            .version("1.0.0")
            .contact(new Contact("","", ""))
            .build();
        }
    
    @Bean
    public Docket api() {

        // configure the entire API
        return new Docket(DocumentationType.SWAGGER_2)
                        .select()
                            .apis(RequestHandlerSelectors.basePackage("com.example.samplerest"))
                            .build()
                        .apiInfo(apiInfo());
    }    

}
```

At this point, you can start the shell application by right clicking on the Application.java class and 
selecting ** Run As | Java Application**.

You can view the Swagger User Interface by browsing this link: http://localhost:8080/swagger-ui.html#/

There will be very little to look at until we create some service entry points with a REST Controller.


## Creating a REST Controller

We'll create a very trivial example to demonstrate various ways to pass parameters into a REST controller.  

Let's start by creating a simple implementation of a Person in **com.example.samplerest.model.Person.jav**:

```java
package com.example.samplerest.model;

public class Person {
	private String id;
	private String name;
	
	// A default constructor is needed to allow
	// a Person object to be received as a JSON
	// parameter
	public Person() {}
	
	public Person( String id, String name ) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}

```

Create a REST controller with some sample entries in **com.example.samplerest.controller.DemoController**:

```java
package com.example.samplerest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.samplerest.model.Person;

@RestController
public class DemoController {
	
	// Example without parameters
	// http://localhost:8080/hello
	@RequestMapping( value="/hello", method=RequestMethod.GET)
	public String hello() {
		return "Hello!";
	}
	
	// Example where parameter is part of the request path
	// http://localhost:8080/get-person/001
	@RequestMapping( value="/get-person/{id}", method=RequestMethod.GET)
	public Person getPerson( @PathVariable( "id" ) String id ) {
		return new Person( id, "Superman" );
	}
	
	// Example where parameters are sent as parameters in URL
	// http://localhost:8080/create-person?id=001&name=WonderWoman
	@RequestMapping( value="/create-person", method=RequestMethod.POST)
	public Person createPerson( @RequestParam( "id" ) String id,
							 @RequestParam( "name" ) String name 	) {
		return new Person( id, name );
	}
	
	// Example for Form submission
	// curl -X POST 
	//      --header 'Content-Type: application/json' 
	//      --header 'Accept: application/json' 
	//      -d '{ "id": "003", "name": "Batman"}' 
	//      'http://localhost:8080/echo-person'
	@RequestMapping( value="/echo-person", method=RequestMethod.POST)
 	public Person  echoPerson( @RequestBody Person person ) {
		return person;
	}
	
}
```

The key thing to note is the annotations used for each parameter type: 

1. ```@PathVariable``` is used when a parameter is part of the request path.
1. ```@ReqeustParam``` is used when parameters are part of the URL
1. ```@RequestBody``` is used to accept JSON objects as parameters.  This is useful for handling forms and large amounts of data.

You can mix and match these as well, for example, you could have both ```@PathVariable``` and ```@RequestBody``` in a single call.

## Setting up .gitignore

At the root of your application directory, create the following **.gitignore**
file to prevent Eclipse specific files from being checked in with **git**.

```shell
.classpath
.project
.settings/
target/
```

## References

1. https://spring.io/docs - The main SpringBoot documentation page
1. https://start.spring.io/ - Spring Initializr
1. https://spring.io/guides/tutorials/bookmarks/ - Detailed information on REST services
1. https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file