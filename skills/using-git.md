## Using Git

Git is currently the most widely adopted source control system.  One of the major differences from its predecessors, SVN and CVS is that instead of directly saving changes to a central repository, you work against a local repository which is a copy of the central version.  To synchronize with other developers on the team, you push your commits to the central repository, and pull other people's changes back down.   

To get a better understanding, read about the basics here: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

## Installation

For Macs, go to the console and type "git".  It will prompt you to load XCode, go ahead and load it.  You could install just Git, but the versions that I found did not have a checksum to check the validity of the download, so the XCode path seemed to be the safest. 

For Windows, download GitBash.  It provides a Unix command line along with Git.

Once you install Git, you'll need to configure your user name and email address:
```
# Set your username
$ git config --global user.name "Your Name Here"

# Set your email address
$ git config --global user.email "your_name@domain.com"

# For windows users
$ git config --global core.autocrlf true

# If you are behind a proxy
$ git config --global http.proxy http://proxy.xxx.xxx:80

```


## Tutorials

Two very good tutorial can be found here:

 1. https://try.github.io/levels/1/challenges/1 - Interactive Version
 1. https://www.atlassian.com/git/tutorials/


A couple of things to note while you are going through the tutorial: 

 1. When you get to cloning a repository, you'll need to create a repository.  Go to GitHub, GitLab or BitBucket and create an account, a repository and get the repository URL.   You'll use that URL for cloning and operating remotely.
 1. You'll generally create your repositories on GitHub, GitLab or BitBucket and clone it.   
 1. Please only use the Centralized Workflow for initially learning how to use Git and understanding the basics on your personal repository.  Do not use this approach on a shared repository.
 1. For workflows, it is best to start with the Feature Branch Workflow.  
 1. Use of the other workflows are advanced topics and the team as a whole should be trained and ready to use these.   They are not recommended as the first step for teams new to Git.


## Cheat Sheet 

Here are the most commonly used commands: 

Cloning a Git Repository:
```
git clone <git URL>
```


Getting the status:
```
git status                   // Gets the current status
git log                      // lists the log of activity
```
**Note:** It is highly recommend that you run `git status` frequently as it 
provide valuable information about the current state of your repository.  This
is especially true while you are getting familiar with the commands.


Branching 
```
git branch                        // lists all local branches
git branch -a                     // lists all local and remote branches

git branch myBranch               // Creates a branch
git branch -d myBranch           // deletes the branch

git push origin myBranch          // pushes the myBranch to origin
git remote prune origin           // This removes stale connections to branches on a remote server
```
**Note:** When you create a branch, you should call **git checkout <branch name>**
to actually switch to that branch.  Typically, you branch from the **master** 
branch.


Adding files to the staging area
```
// then add and commit
git add .
git add -u        // -u this picks up deletions
```

Committing files to the local repository
```
git commit                                         // if message is not provided, you'll be prompted to enter comments, usually in the VIM editor but this can be changed.
git commit -m "Your commit comments go here"
```

Pushing to the main repository
```shell
git push origin <your branch>

```

Pulling the latest master from the main repository
```
git checkout master
git pull

# Checkout a branch from the main repository
git checkout --track origin -b <branch name>
```

Comparing Versions
```
git diff                        // compares unstaged to staged
git diff --staged               // compares staged with HEAD (Version of last commit on branch)
git diff HEAD                   // compares unstaged with HEAD
```

Tagging
```
git tag v1.0                                            // Basic Tag
git tag -a "v1.0" -m "My first version."                // Annotated tagging
git tag                                                 // Lists all tags
git tag -l "v1.*"                                       // Lists a selected set of tags
git show v1.0                                           // displays detailed information on specific tag
git push origin v1.0                                    // pushes the tag to the repository

git tag -d v1.0                                         // delete tag locally
git push -d origin v1.0                                 // delete tag at remote repository

git fetch origin --tags                                 // fetch tags from origin
```


## Common Tasks

### Clone a Repository

You'll either create a repository or be given a link to the repository.   The first think you want to do is clone a copy of the 
repository on your local hard drive.   The default location is under the /git directory in your home directory.

At your local machine, do the following: 

```
// If this is a completely new installation, you'll need to create that directory.
cd ~/git

git clone <repository url>
```

### Importing a Git Project into Eclipse

 1. Right click in Package Explorer and select Import... | Import... | Maven | Existing Maven Projects
 1. Browse to directory of project.  It should have a pom.xml file in the root.
 1. Click finish

### Creating a new project and pushing it to the repository

Go to the directory for your project and add a .gitignore file.   For Dart, it would look like this: 

```
# https://www.dartlang.org/guides/libraries/private-files

.DS_Store

build/
packages/
.buildlog
.packages
.project
.pub/

doc/api/

*.dart.js
*.info.json      # Produced by the --dump-info flag.
*.js             # When generated by dart2js. Don't specify *.js if your
                 # project includes source files written in JavaScript.
*.js_
*.js.deps
*.js.map
```


Then type the following commands.

```
// initialize git
git init

// view the files you are about to check in.
git status

// add and commit your files
git add .
git commit -m "Initial commit"
```

Create a remote repository and capture the URL used to clone it.

```
// Specify a remote repository named origin
git remote add origin <repo-address>

```

### Working on a Feature/Fix

Always create a new branch when you work on a feature or fix.  Never work directly off of the master.  The branch name should indicate what is being done.  
In the following example, we'll call it fix-help-window.

```
git checkout master                 // this will switch you to the master branch
git pull                            // this pulls down the latest changes to your local copy

// if you are creating a branch from Jira: 
git checkout --track origin -b <branch name>

// or if creating a branch locally
git branch fix-help-window          // creates a new branch starting from the current version of master you just pulled down.
git checkout fix-help-window        // switches you over to the new branch.   Important: if you don't do this, you still are on the master branch!

// Make the changes/additions, etc. 
git status                          // Periodically call this to see the status of your current uncommitted files.
git add .                           // adds all changes.  If you deleted any files, you need to use the -u flag.
git commit                          // Commit your changes.

// Push your updated branch to the central repository
git push origin fix-help-window

// Go to GitHub and create a pull request on the branch you just pushed.

// To update your local master branch:
git checkout master
git pull

```

## Recovering from Errors

Rolling back changes:
```
git reset        // Rolls back the last commit - use locally as this is just repoints the branch to the previous commit.
git revert       // This must be used to roll back changes already sent out to a remote repository.  It creates a new version of the last commit
```

Resolving merge conflicts
```
// The most common merge conflicts occur when the master branch (or the branch you based your checkout on) 
// has been updated since you first checked out.  In that case, you need to catch up your version 
// of the master branch, and then merge the changes into your branch.

// remove all unstaged files.  If you have work in progress, add them to staging and call git stash.

git stash               // only if you have uncommitted stuff in staging.  
git stash list          // optional so you can view what you just stashed.


git checkout master
git pull                // update master with latest changes

// switch to your branch and merge in master
git checkout <your branch>

// if you stashed, this will pop your files off of the stash list.
git stash pop           

git merge master

// Once you merge, any merge conflicts will be marked in the affected source files.  These files must be reviewed and manually corrected by you.  Once you are done, commit them and push your 
// branch as you normally would. 

```


If you want to hard reset master to origin/master 

```
git checkout master
git reset --hard origin/master 
```
**Note:** This is a last resort solution.  If you are doing this often, you should
figure out why you are getting into this situation.

### Recommended Project Directory Structure

The following is the recommended directory structure for your application:
```
~/git/your-app
           +------.gitignore             // The version listed below
           +------ /ui                   // Your Angular App
                    +------- .gitignore  // Created by Angular CLI
           +------ /rest                 // Your Bootstrap/Swagger App
```

### Recommended .gitignore 

This is a useful example derived from an autogenerated by the Angular CLI.  
It is intended to be placed at the root of your project where the Angular
UI application and the REST applications are in subdirectories.



```
# See http://help.github.com/ignore-files/ for more about ignoring files.

# compiled output
dist/
tmp/
out-tsc/

# dependencies
node_modules/

# IDEs and editors
.idea/
.project
.classpath
.c9/
*.launch
.settings/
*.sublime-workspace

# IDE - VSCode
.vscode/*
!.vscode/settings.json
!.vscode/tasks.json
!.vscode/launch.json
!.vscode/extensions.json

# misc
.sass-cache/
connect.lock/
coverage/
libpeerconnection.log/
npm-debug.log
testem.log
typings/

# e2e
e2e/*.js
e2e/*.map

# System Files
.DS_Store
Thumbs.db

```

## Resources

 1. https://git-scm.com/book/en/v2
 1. https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf
 1. https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow/

