# Jasmine testing framework
The most popular framework for "BDD" (or "Behavior Driven Development") tests is Jasmine.
We won't get into the nitty gritty of **BDD**, nor will we focus on it as a driving principle.
But it may help to understand why the test are written in the manner they are written.

The general idea is this:

 - **Describe** the behavior you want, then
 - Write the tests to ensure that **it** does.

## Step 1: Describe

```javascript
// "describe" function is also called "suite" in documentation
describe('Search', function(){
  
  // "it" function is also called "spec" in documentation
  it("should start when user presses Enter key");
  
  describe('results', function() {
    it("should return in a list");
    it("should be sorted by xyz");
  });
  
  describe('errors', function(){
    it('should display friendly message');
  });
  
  // ... etc
});
```

You would run a test like the one above with a command like, `npm test`, or `jasmine-node` (Jasmine does not come
with a test-runner--you usually have to use one installed on your node or Angular project, or use the stand-alone
runner `jasmine-node`.)

Depending on the test reporter configured, the stubbed tests above can be reported in this manner:

```shell
 Search should start when user presses Enter key
 Search results should return in a list
 Search results should be sorted by xyz
 Search errors should display friendly message
```

or more commonly, like this:

```shell
 Search 
     should start when user presses Enter key
     results 
         should return in a list
         should be sorted by xyz
     errors 
         should display friendly message
```
 
In other words, the stubbed test cases read like normal sentences describing the behavior of the system.
Stubbing your tests this way before you start writing any code at all is the "describe" step,
or "writing acceptance tests" step, in Behavior Driven Development.
It helps you define the scope of your work, and gives you a "TODO" list of functionality to be developed.

### Test Runners
The Jasmine framework helps you write tests while allowing you to use different runners depending upon your context.
In Angular projects, built using `@angular/cli`, tests are run using **Karma** (for unit tests), and **Protractor** 
for end-to-end tests.

## Step 2: Write Tests
If you are following BDD (or TDD), in which tests are written _before_ coding, then you would write your tests at this step.

Whether you follow BDD/TDD, or write tests after your code is written, it is _very important_ that every test you write **FAILS** 
before you make it _PASS_. This is to ensure that your test actually _tests_ something, instead of returing a false positive.

  >Writing unit tests that do not fail first is inherently a _waste of time_: it wastes your time to write a test that you
    are not sure actually catches bugs, and it wastes other developers' time if they trust your test to do its job correctly.

When you follow BDD/TDD, your tests fail automatically (because you haven't written any code yet).
When you write tests after writing code, you must go into your code and create the type of bug your test is designed to catch
and make sure it fails accordingly, before re-fixing your code to work again.

### Test Context, or `this` in a Javascript test
Jasmine (and many test frameworks) support a "test case" context, in which the relevant information about a specific test
(or "spec") is always initialized and destroyed within the lifecycle of a single spec. This prevents variables you set on one test
spec from polluting another.

Let's see how this works:
```javascript
describe("Learning Jasmine", function(){
  
  beforeEach(function(){
    this.bar = "beforeEach!";
  });
  
  afterEach(function(){
    this.foo = "afterEach";
  });
  
  it("should be painless", function(){
    this.papa = "inside it";
    expect(this.mama).toBeUndefined();
  });
  
  it("should not suck", function(){
    this.mama = "inside another it";
    expect(this.papa).toBeUndefined();
  });
  
  it("sucks using fat arrow", () => {
    expect(this.bar).toEqual("beforeEach!");
  });
});
```
When Jasmine runs, this is what it does (simplified):

```javascript
// "should be painless" spec
userContext = {}; // <-- new empty object
userContext.bar = "beforeEach!";
userContext.papa = "inside it";
expect(userContext.mama).toBeUndefined(); // <-- passes
userContext.foo = "afterEach";
  
// "should not suck" spec
userContext = {}; // <-- new empty object (no leaking between specs)
userContext.bar = "beforeEach!";
userContext.mama = "inside another it";
expect(userContext.papa).toBeUndefined(); // <-- passes
userContext.foo = "afterEach"
  
// "sucks using fat arrow" spec
userContext = {}; // <-- new empty object
userContext.bar = "beforeEach!";
expect(this.bar).toEqual("beforeEach!"); // <-- fails because fat arrow doesn't allow for Jasmine's `this` (which is`userContext` under the covers)
userContext.foo = "afterEach"; // <-- afterEach always runs even if the spec fails
```

It should be noted that Jasmine tests are typically run in random order to help you spot tests that are incorrectly
dependent upon other tests.

### Excluding Tests
You would exclude tests if you need to ignore a spec implementation (if there is no implementation, the test is already ignored).
These excluded tests report just like the pending (stubbed) tests:

```javascript
xdescribe("exclude an entire suite", function(){ 
  //... 
});

describe("exclude some specs", function(){
  it("stubbed");
  xit("excluded");
  xit("another excluded");
});
```

### Marking tests as pending
You can stub tests (tests with no body), or you can execute the `pending()` function to mark a test as pending:

```javascript

describe("some pending tests", function(){
  
  it("this is a pending test with no body");
  
  it("this is a pending test with a body", function(){
    // test code
    pending();
  });
  
  xit("this is an excluded test that will look pending", function(){
    // test code
  });
});
```

### Focusing Tests
You would focus a test or tests if you wanted to run **ONLY** those tests. 
Typically you do this because your test suite has gotten large, or the suite takes too long to run.

```javascript
fdescribe("focus on an entire suite", function() {
  // ...
});

describe("focus on some specs", function(){
  it("ignored", function(){
    fail("not yet implemented");
  });
  fit("focused", function(){
    // ...
  });
  fit("another focused", function(){
    // ...
  });
});
```

You can focus MANY tests across many files. All tests marked with the `f` character (e.g., `fdescribe` and/or `fit`) 
will be run.

Don't forget to remove these qualifiers if you want the entire suite of tests to be run.

## Resources
 * [Jasmine Documentation](https://jasmine.github.io/2.0/introduction.html)
 * [Better Jasmine Tests with `this`](https://gist.github.com/traviskaufman/11131303)
