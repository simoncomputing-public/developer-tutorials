# React Tutorial

This is a set of references to review to develop in React.

## JavaScript

Here are links of things that are helpful for developers coming from Java:

1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript
1. https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects
1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import

## React
1. https://reactjs.org/
1. https://reactjs.org/tutorial/tutorial.html
1. http://jamesknelson.com/learn-raw-react-ridiculously-simple-forms/
1. https://blog.risingstack.com/react-js-best-practices-for-2016/

## Redux
1. http://redux.js.org/
1. https://www.toptal.com/react/react-redux-and-immutablejs
1. https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md
1. https://mva.microsoft.com/en-us/training-courses/using-react-with-redux-17730?l=

## Material Design Lite
1. https://material-ui-next.com/
1. https://material-ui-next.com/getting-started/installation/

## Router Information
1. https://reacttraining.com/react-router/web/guides/philosophy
1. https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf
