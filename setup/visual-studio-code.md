# Visual Studio Code

This is a free editor that supports both JavaScript and TypeScript.  It is ideal
for handling the coding for Angular, HTML, CSS, etc. 

Things to like about it: 

 1. User interface is very similar to Sublime, so if you are familiar with that
    this will be a breeze to use.
 1. Eclipse editing feature that allows you to move whole blocks of code with 
    [Alt/Option-UpKey] and [Alt/Option-DownKey] work.
 1. Friendlier behavior for [Home] and [End] take you to the beginning and 
    end of line on Mac, vs beginning and ending of document.
 
To get the download, go to: https://code.visualstudio.com/ or [Visual Studio Code for Mac](https://code.visualstudio.com/docs/setup/mac)

The first two videos on that page will provide a useful introduction to the 
use of those tools.


## Command Line
You can also run VS Code from the terminal by typing 'code' after adding it to the path:

 1. Launch VS Code.
 1. Open the Command Palette (⇧⌘P) and type 'shell command' to find the `Shell Command: Install 'code' command in PATH command`.
 1. Run it and it will report that `code` has been added to path.

Next time you open a terminal window and navigate to a project directory, you can 
execute `code .` to open the project in Visual Studio Code.
