# Directory Structure
As you begin setting up your environment, installing software, and creating **git** repositories, you should try to 
follow these guidelines so that the team can easily collaborate knowing that your file directory structure mimics theirs.

## General

### The `~/Dvl` or `C:\Dvl` directory for Tools
Use this directory for the installation of tools (e.g., Apache Tomcat, Maven, H2 Database, etc).

### The `~/git` or `C:\git` directory for Projects
Use this directory for your software projects that will be under version control in **git** repositories.

When you setup Eclipse, you may want to make this your default workspace if you expect to be using **git** for version control.
