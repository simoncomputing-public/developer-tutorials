## Using Yarn

Yarn is a package manager that resolves problems with NPM while staying compatible with the NPM registry.  Yarn's enhanced functionality includes:

 1. Generates a version lock file to make it possible for everyone on the team to use the same version of all libraries
 1. Faster performance by caching downloads and parallelizing download operations
 1. Uses checksum to verify integrity of download


**To install:**
```shell
npm install yarn -g
```


The installation can be found at: **~/.yarn**
The global yarn cacke can be found at **~//Library/Caches/Yarn**


### Cheat Sheet

These are the equivalent commands between NPM and Yarn.


| NPM                              | Yarn                    | Comments |
| -------------------------------- | ----------------------- | -------- |
| npm init                               | yarn init                     | Start a new project |
|                                        |                               |                     |
| npm install                            | yarn                          | Reads package.json and installs all dependencies |
| npm install --save [package]           | *yarn add [package]*            | Installs a package.  Yarn **does not add** the dependency to package.json |
| npm install --save [package]@[version] | *yarn add [package]@[version]*  | Installs a package.  Yarn **does not add** the dependency to package.json |
| npm install --save [package]@[tag]     | *yarn add [package]@[tag]*      | Installs a package.  Yarn **does not add** the dependency to package.json |
||||
|npm install --save-dev [package]        | *yarn add [package] [--dev/-D]* | Install package.  Yarn **does not add** the development dependency |
||||
| npm install --global [package]         | yarn global add [package]     | Globally installs package |
||||
| npm uninstall [package]                      | yarn remove [package]         | uninstalls package |
| npm uninstall [package] --save [package]     | yarn remove [package]         | uninstalls package |
| npm uninstall [package] --save-dev [package] | yarn remove [package]         | uninstalls package |
||||
| npm update [package]                        | yarn upgrade [package]                    | Cleans out cache |
||||
| npm cache clean                        | yarn cache clean                    | Cleans out cache |
||||
| rm -rf node_modules && npm install     | yarn upgrade                        | cleans and reinstalls node_modules |
||||
||||
| npm config set &lt;key&gt; &lt;value&gt; [-g&#124;--global]     | yarn config set &lt;key&gt; &lt;value&gt; [-g&#124;--global] | cleans and reinstalls node_modules |
||||
| npm view &lt;package&gt; [&lt;field&gt;]           | yarn info &lt;package&gt; [&lt;field&gt;] | View package information |
| npm view &lt;package&gt; version             | yarn info &lt;package&gt; version   | View package version    |

## Gotchas

1. Yarn should always be run from the root of your application's directory structure.   Yarn searches for the **package.json** file and the **node_modules/** directory.  If it doesn't find 
them it will proceed to create a **node_modules/** directory in the directory you are in, so you'll start to have these files everywhere.

1. Yarn does not update package.json on **yarn add**, for this reason, it is highly recommended that you use **npm install** instead.


### References

 1. https://yarnpkg.com/en/docs/migrating-from-npm
 1. https://yarnpkg.com/en/docs/getting-started
 1. https://docs.npmjs.com/cli/config
 1. https://docs.npmjs.com/files/package.json
 1. https://yarnpkg.com/en/docs/publishing-a-package
 1. https://hackernoon.com/using-yarn-with-docker-c116ad289d56#.ufka2t9e8
