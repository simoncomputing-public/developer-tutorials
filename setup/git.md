# Installing Git on Mac

See if you can run git:
```shell
git --version

# You should see something like this:
git version 2.11.0 (Apple Git-81)
```

If not or if you get an error like: 

```shell
xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun.
```
You'll need to install it. 

You can either install the full XCode installation: 

```shell
xcode-select --install
```
or go to https://sourceforge.net/projects/git-osx-installer/files/ to get the minimal installation.

# Installing Git on Windows

GitBash works great for windows users and it gives you a Unix like command line, which will be very helpful because most commands given in this tuturial are Unix based.

See this link: https://git-for-windows.github.io/
