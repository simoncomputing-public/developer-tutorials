# Eclipse for Java and Angular 2 Development 

These instructions are based on installation of the Eclipse Neon.


## Download Eclipse 

 1. Go to the http://www.eclipse.org/downloads/eclipse-packages/
 1. Select the **Eclipse IDE for Java EE Developers**
 1. Select the **Eclipse SDK** download for your system (select 64 bit)

## Initial Installation 

 1. Copy the installation zip file into ~/Dvl and extract the file into that directory.  You can simply click on the zip file to uncompress it into the current directory.
 1. To start, go into the subdirectory and click on eclipse.app.

-----
## Initial Settings 

### Code Style Settings 
These settings affect how code is styled, and it is in the interest of the team to make sure all use the same settings.

 1. **Trailing White Space**
    * We set this so it is easy to see visually that there are trailing spaces (we want to get rid of them where possible)
    * Go to **Eclipse** | **Preferences** | **General** | **Editors** | **Text Editors**
    * Check the box **Show whitespace characters**
     i. ''Optionally'' click **configure visibility** and choose the settings you want, keeping the ability to see ''trailing spaces'' as well as ''Tabs''
       ![Configure Visibility in Eclipse](./resources/Eclipse_configure-visibility.png)
       * You may prefer to keep the defaults and decide later to modify these settings if you find the editor view very cluttered.
       * ''Optionally'', if you like to see line numbers, select **Show line numbers** here
 

 1. **2 or 4 spaces instead of Tab**
    We use spaces instead of Tab. Most modern IDEs and coding editors will translate a Tab keystroke into spaces if you configure it.
    We set the indent depth to 4 spaces by default in java files. Because rapid development techniques often utilize code generation, we allow 2 spaces in most other file types (because code generators for Javascript, CSS, HTML, etc. typically indent 2 spaces).
    * **General**
      * Go to **General** | **Editors** | **Text Editors**
        * **Displayed tab width**: "`4`"
        * check **Insert spaces for tabs**
    * **Java**
      * Go to **Preferences** | **Java** | **Code Style** | **Formatter**
      * Click **Edit** on the active profile
        * **Profile name**: "`Eclipse [built-in] - spaces only`" (you must rename in order to save changes)
        * **Tab policy**: "`Spaces Only`"
        * **Indentation size**: "`4`"
        * **Tab size**: "`4`"
      * Click **Ok**
    * **JavaScript**
      * follow the same steps as the Java Formatter, except set indentation size to "`2`" instead of "`4`"
    * **TypeScript**
      * Go to **!TypeScript** | **Formatter**
        * **Indent size**: "`2`"
        * **Tab size**: "`2`"
    * **JSON**
      * Go to **JSON** | **JSON Files** | **Editor**
        * Select "`Indent using spaces`"
        * **Indentation size**: "`2`"
    * **XML Files**
      * Go to **XML** | **XML Files** | **Editor**
        * Select "`Indent using spaces`"
        * **Indentation size**: "`2`"
    * **YAML Files**
      * Go to **Web** | **YAML Files**
        * **Spaces per tab**: "`2`"
    * **HTML Files**
      * Go to **Web** | **HTML Files** | **Editor**
        * Select "`Indent using spaces`"
        * **Indentation size**: "`2`"
    * **CSS Files**
      * follow the same steps as the HTML Files

 1. Click **Apply**


-----

## Eclipse Maven Setup

  1. Ensure that you have already installed the command line version of Maven.  The instructions can be found here: [Maven](./maven-setup.md).  
  1. For every new workspace, you must set your Maven installation location before creating new projects.  If this is not done, your projects will be pointing to the default repository, which  is different from the location for the command line version:

   * From the Eclipse menu, select **Preferences | Maven | Installation | Uncheck Embedded**. 
   * Click Add and specify the Maven location.
   * Click OK.
  1. Set global settings in Eclipse:
   * Go to **Preferences | Maven | User Settings**.
   * Set Global Settings to ~/Dvl/apache-maven-3.3.9/conf/settings.xml.
  1. Note that you should use the Internet Explorer browser and not the Eclipse browser to run.  Sometimes it does not work properly.   


-----
## Importing Projects

After setting up SourceTree and cloning the desired projects:
 1. Right-click Project Explorer and select **Import | Import...**.
 2. Select existing Maven project.
 3. Navigate to your cloned repository for the desired project and click OK.

This will set up your project based in the repository external to the Eclipse workspace.  This will allow you to switch workspaces and still reference the same projects if necessary.


-----
## Tweaking Content Assist (Intellisense)
After you work with Eclipse for a while, you may find that the Content Assist is not helping you in a way that suits your personal coding style. For example, intellisense auto-completion may cause you to repeatedly backspace over content that Eclipse added "helpfully" for you.

You can tweak these settings by going to **Preferences** | **Java** | **Editor** | **Content Assist**.

![Content Assist](./resources/eclipse-content-assist.png)
