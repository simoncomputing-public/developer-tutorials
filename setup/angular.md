# Install (global) @angular/cli

We'll be using [Angular CLI](https://github.com/angular/angular-cli) to scaffold Angular projects, so let's install it:

```shell
// see what packages are installed
npm ls -g --depth=0
  
// if you don't have @angular/cli@1.0.0...
npm install @angular/cli@1.0.0 -g
  
// If it asks, say "yes" to using `yarn` as the dependency manager

// Confirm installation by checking version
ng --version

```