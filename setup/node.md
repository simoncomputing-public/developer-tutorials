#### Install (global) Node Version Manager and NodeJS

Node Version Manager (or `nvm`) is a popular tool used to manage multiple versions of NodeJS on the same computer.

`nvm` provides the ability to run different versions of NodeJS in different terminal environments at the
same time (useful if you maintain or work on multiple NodeJS projects).

One thing to remember is that global modules installed at one version of node are not "automatically carried" over to new
versions of NodeJS that you install at a later date (you have to install the modules again). 

This has its benefits as well: modules you installed at an earlier version of NodeJS are still associated with that
version, so when you switch back to that version, you do not need to worry about dependency issues arising from version
conflicts.

Run the following steps to install `nvm` (see [creationix/nvm on GitHub](https://github.com/creationix/nvm/blob/master/README.md) for details):

```shell
// see if you have it installed
nvm --version
  
// on MacOSX:
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
  
// to make it active without closing the terminal:
source ~/.bash_profile
  
// install node
nvm install stable
nvm use stable
  
// list versions
nvm list
```

#### Install (global) Node Package Managers

Node Package Manager (or `npm`) comes with NodeJS, but might not be the latest copy. We'll be installing the latest copy,
then using `npm` to install `yarn`, the package manager that [Angular CLI](https://github.com/angular/angular-cli) uses
by default.

```shell
// check the version
npm --version
  
// if your version is not 4.5 or higher:
npm install npm -g
  
```