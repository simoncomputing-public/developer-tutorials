# Maven

Maven is a software build tool that simplifies the build process.   It's two most significant features that sets it apart from build tools like Ant and Make are: 

 1. Standardized Project Structures: Maven design favors "convention over configuration".  By establishing a standard project structure, a Maven project can be built without configuring the location of all input and output files in the build process.   This eliminates a lot of configuration that traditionally was required in other build tools.  It also makes every project look the same, so developers from one Maven project can easily understand other Maven projects.   

 1. Library Management:  Maven projects must explicitly declare all libraries required for the build by name and version.  Maven then retrieves the appropriate libraries from one or more Maven repositories.  There are public Maven repositories that house open source libraries, and organizations can stand up their own repositories for internally created and used libraries.   

Maven has quickly becoming the de-facto standard for building applications.


## Installing Maven

Maven comes pre-installed with Eclipse past Kepler.

 1. Go to http://maven.apache.org/download.cgi and download the latest release of Maven.  
 1. Extract the file into c:\dvl or ~/dvl.  This should result in a directory something like this: apache-maven-3.1.1
 1. For Macs: you'll need to go to the console and add the following lines to the ~/.bash_profile file:

```
  export M2=~/Dvl/apache-maven-x.x.x
  PATH=$M2/bin:$PATH
```

If a PATH= statement already exists, just prepend the existing path with **$M2/bin:**

 1. For PCs, you'll need to right click on Computer and select Set Properties | Advanced | some option that let's you update the local environment variables.  Prepend the existing Path statement with **c:\dvl\%M2%\bin;**

## Building the war file from the command line 
Later, when you have a maven project set up, you can go to the commandline and execute the following to build a war file.

 1. Change directory into the project directory.  It should contain your `pom.xml` file
 1. `mvn clean install` 
 1. `mvn clean install -Dmaven.test.skip=true`

## More on Maven in Eclipse 

In the future you will spend some time looking for dependencies to put in your pom.xml. Two resources for this are:
  1. The Central Repository. 
    * http://search.maven.org
    * http://central.maven.org
  1. Eclipse Project Tool:
    * Right click on your project. 
    * Go to Maven, click "Add Dependency". 
    * Search for the dependency that you want, chances are it will be in here. 

Sometimes your Maven Project in eclipse will show the red 'X' error, when there is no clear problem. If this is the
case try updating your project by:
  1. Right click your project. 
  1. Go to Maven and select 'Update Project...'
  1. The defaults for this action are fine. Update the project.  
  
**See also:**

 1. [Building a Maven Jar](../skills/maven-jar.md)

## Putting Projects in the Central Repository

 1. http://maven.apache.org/guides/mini/guide-central-repository-upload.html
 1. http://central.sonatype.org/pages/working-with-pgp-signatures.html

## References

 * http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html
 * http://books.sonatype.com/mvnref-book/reference/public-book.html
 
