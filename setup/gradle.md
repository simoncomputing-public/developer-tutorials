# Setting up Gradle

## Mac Setup

Manually install Gradle by following these steps:

1. Download the latest release from [here](https://gradle.org/releases/).
1. Save to ~/Dvl.
1. unzip -d ~/Dvl/gradle gradle-4.2.1-bin.zip
1. Validate file: ```shasum -a 256 gradle-4.2.1-bin.zip ```
1. Put Gradle in your path in **.bash_profile** like this: 

```shell
export PATH=$PATH:~/Dvl/gradle/gradle-4.2.1/bin
```

## Validate

```shell
source ~./bash_profile
gradle -v
```

## Reference

1. https://gradle.org/
